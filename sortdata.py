from fuzzywuzzy import fuzz
import numpy as np
import sys

def GetListOfPhrases(path_to_data):
    with open(path_to_data) as f:
        phrases = []
        for row in f:
            phrases.append(row.lower()[2:])
    return phrases

def WriteToFile(path_to_file, phrases):
    with open(path_to_file, "w+") as f:
        for row in phrases:
            string = "- " + row
            f.write(string)
        f.close()
    return

def SortByRatio(list_of_phrases, toolbar_width = 0):
    # Progress Bar
    if toolbar_width != 0:
        print("Sorting By Ratio: Initiated")
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        iteration = 0
        len_list = len(list_of_phrases)
        int_progress = 0
    phrases = list_of_phrases.copy()
    if "" in phrases:
        phrases.remove("")
    shortest_phrase = min(phrases, key=len)
    sorted_list = [shortest_phrase]
    phrases.remove(shortest_phrase)
    while len(phrases) > 0:
        sim = 0
        sim_idx = 0
        for i in range(len(phrases)):
            Ratio = fuzz.ratio(shortest_phrase,phrases[i])
            if sim < Ratio:
                sim_idx = i
                sim = Ratio
        sorted_list.append(phrases.pop(sim_idx))
        if toolbar_width != 0:
            iteration += 1
            if round(iteration/len_list*toolbar_width) > int_progress:
                sys.stdout.write("-")
                sys.stdout.flush()
                int_progress = round(iteration/len_list*toolbar_width)
    if toolbar_width != 0:
        sys.stdout.write("-\n")
        print("Sorting By Ratio: Finished")
    return sorted_list

def SortByPartialRatio(list_of_phrases, toolbar_width = 0):
    # Progress Bar
    if toolbar_width != 0:
        print("Sorting By Partial Ratio: Initiated")
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        iteration = 0
        len_list = len(list_of_phrases)
        int_progress = 0
    phrases = list_of_phrases.copy()
    if "" in phrases:
        phrases.remove("")
    shortest_phrase = min(phrases, key=len)
    sorted_list = [shortest_phrase]
    phrases.remove(shortest_phrase)
    while len(phrases) > 0:
        sim = 0
        sim_idx = 0
        for i in range(len(phrases)):
            Partial_Ratio = fuzz.partial_ratio(shortest_phrase,phrases[i])
            if sim < Partial_Ratio:
                sim_idx = i
                sim = Partial_Ratio
        sorted_list.append(phrases.pop(sim_idx))
        if toolbar_width != 0:
            iteration += 1
            if round(iteration/len_list*toolbar_width) > int_progress:
                sys.stdout.write("-")
                sys.stdout.flush()
                int_progress = round(iteration/len_list*toolbar_width)
    if toolbar_width != 0:
        sys.stdout.write("-\n")
        print("Sorting By Partial Ratio: Finished")
    return sorted_list

def SortByTokenSortRatio(list_of_phrases, toolbar_width=0):
    if toolbar_width != 0:
        # Progress Bar
        print("Sorting By Token Sort Ratio: Initiated")
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        iteration = 0
        len_list = len(list_of_phrases)
        int_progress = 0
    phrases = list_of_phrases.copy()
    if "" in phrases:
        phrases.remove("")
    shortest_phrase = min(phrases, key=len)
    sorted_list = [shortest_phrase]
    phrases.remove(shortest_phrase)
    while len(phrases) > 0:
        sim = 0
        sim_idx = 0
        for i in range(len(phrases)):
            Token_Sort_Ratio = fuzz.token_sort_ratio(shortest_phrase,phrases[i])
            if sim < Token_Sort_Ratio:
                sim_idx = i
                sim = Token_Sort_Ratio
        sorted_list.append(phrases.pop(sim_idx))
        if toolbar_width != 0:
            iteration += 1
            if round(iteration/len_list*toolbar_width) > int_progress:
                sys.stdout.write("-")
                sys.stdout.flush()
                int_progress = round(iteration/len_list*toolbar_width)
    if toolbar_width != 0:
        sys.stdout.write("-\n")
        print("Sorting By Token Sort Ratio: Finished")
    return sorted_list

def SortByTokenSetRatio(list_of_phrases, toolbar_width=0):
    # Progress Bar
    if toolbar_width != 0:
        print("Sorting By Token Set Ratio: Initiated")
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        iteration = 0
        len_list = len(list_of_phrases)
        int_progress = 0
    phrases = list_of_phrases.copy()
    if "" in phrases:
        phrases.remove("")
    shortest_phrase = min(phrases, key=len)
    sorted_list = [shortest_phrase]
    phrases.remove(shortest_phrase)
    while len(phrases) > 0:
        sim = 0
        sim_idx = 0
        for i in range(len(phrases)):
            Token_Set_Ratio = fuzz.token_set_ratio(shortest_phrase,phrases[i])
            if sim < Token_Set_Ratio:
                sim_idx = i
                sim = Token_Set_Ratio
        sorted_list.append(phrases.pop(sim_idx))
        if toolbar_width != 0:
            iteration += 1
            if round(iteration/len_list*toolbar_width) > int_progress:
                sys.stdout.write("-")
                sys.stdout.flush()
                int_progress = round(iteration/len_list*toolbar_width)
    if toolbar_width != 0:
        sys.stdout.write("-\n")
        print("Sorting By Token Set Ratio: Finished")
    return sorted_list

def SortByMeanRatio(list_of_phrases, toolbar_width=0):
    # Progress Bar
    if toolbar_width != 0:
        print("Sorting By Mean Ratio: Initiated")
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        iteration = 0
        len_list = len(list_of_phrases)
        int_progress = 0
    phrases = list_of_phrases.copy()
    if "" in phrases:
        phrases.remove("")
    shortest_phrase = min(phrases, key=len)
    sorted_list = [shortest_phrase]
    phrases.remove(shortest_phrase)
    while len(phrases) > 0:
        sim = 0
        sim_idx = 0
        for i in range(len(phrases)):
            Ratio = fuzz.ratio(shortest_phrase,phrases[i])
            Partial_Ratio = fuzz.partial_ratio(shortest_phrase,phrases[i])
            Token_Sort_Ratio = fuzz.token_sort_ratio(shortest_phrase,phrases[i])
            Token_Set_Ratio = fuzz.token_set_ratio(shortest_phrase,phrases[i])
            mean = (Ratio+Partial_Ratio+Token_Set_Ratio+Token_Sort_Ratio)/4 
            if sim < mean:
                sim_idx = i
                sim = mean
        sorted_list.append(phrases.pop(sim_idx))
        if toolbar_width != 0:
            iteration += 1
            if round(iteration/len_list*toolbar_width) > int_progress:
                sys.stdout.write("-")
                sys.stdout.flush()
                int_progress = round(iteration/len_list*toolbar_width)
    if toolbar_width != 0:
        sys.stdout.write("-\n")
        print("Sorting By Mean Ratio: Finished")
    return sorted_list

if __name__ == "__main__":
    path_to_data = "unclassified_data.md"
    phrases = GetListOfPhrases(path_to_data)
    sortedbyratio = SortByRatio(phrases, 100)
    sortedbypartialratio = SortByPartialRatio(phrases, 100)
    sortedbytokensortratio = SortByTokenSortRatio(phrases, 100)
    sortedbytokensetratio = SortByTokenSetRatio(phrases, 100)
    sortedbymean = SortByMeanRatio(phrases, 100)
    WriteToFile("sorted_by_mean.md", sortedbymean)
    WriteToFile("sorted_by_partial_ratio.md", sortedbypartialratio)
    WriteToFile("sorted_by_token_sort_ratio.md", sortedbytokensortratio)
    WriteToFile("sorted_by_token_set_ratio.md", sortedbytokensetratio)
    #for i in range(len(phrases)):
        #print(sortedbyratio[i])
        #print(sortedbypartialratio[i])
        #print(sortedbytokensortratio[i])
        #print(sortedbyratio[i])
        #print(sortedbymean[i])
        #print(" ",sortedbyratio[i],sortedbypartialratio[i],sortedbytokensortratio[i],sortedbytokensetratio[i],sortedbymean[i])
        #print("-------------------------")