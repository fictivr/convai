import requests
import json
import re
# Instructions:
# run rasa server with: rasa run --enable-api

def GetResponsesFromFile(path_to_file):
    with open(path_to_file) as f:
        data = json.load(f)
    responses = []
    for scenario in data:
        try:
            for line in scenario["lines"]:
                r = requests.post('http://localhost:5005/model/parse', json={"text": line})
                responses.append(r.json())
        except requests.exceptions.RequestException:
            print("Error: No connection to RASA server")
        except:
            print(scenario["error"])
    return responses

def PrintData(responses, option=0, min_confidence=0.9):
    if option == 0:
        for response in responses:
            print(response["intent"]["confidence"])
    elif option == 1:
        for response in responses:
            print(response["text"])
    elif option == 2:
        for response in responses:
            if response["intent"]["confidence"] < min_confidence:
                intent_conf = response["intent"]["confidence"]
                if response["intent"]["name"] == "chitchat":
                    resp_sel = response["response_selector"]["chitchat"]["full_retrieval_intent"][8:]
                    resp_conf = response["response_selector"]["chitchat"]["response"]["confidence"]
                    print("chitchat: ", str(intent_conf)," ",resp_sel,": " ,str(resp_conf))
                else:
                    print(response["intent"]["name"], ": ", str(intent_conf))
                for intent in response["intent_ranking"][1:]:
                    print(intent["name"], ": ", str(intent["confidence"]))
                print(response["text"])
                print("------------------")
    elif option == 3:
        for response in responses:
            if response["intent"]["name"] == "chitchat":
                #for item in response:
                #    print(item)
                print(response["intent"]["confidence"])
                for item in response["response_selector"]["chitchat"]["ranking"]:
                    print(item["confidence"])
                print("____")
                
def MeanConfidence(responses):
    dummy = 0
    for response in responses:
        dummy += response["intent"]["confidence"]
    try:
        mean = dummy/len(responses)
    except ZeroDivisionError:
        print("No response")
        mean = 0
    return mean

def PrintIntentRanking(response):
    #print(response["intent"]["name"], ": ", response["intent"]["confidence"])
    dummy = 1
    for item in response["intent_ranking"]:
        print(str(dummy),". ", item["name"], ":", item["confidence"])
        dummy += 1

def SplitInputIntoTwo(text):
    text_as_list = text.split(" ")
    confidence_sum = 0
    for word_idx in range(1, len(text_as_list)):
        s1 = text_as_list[:word_idx]
        s2 = text_as_list[word_idx:]
        s1 = "".join(i+" " for i in s1)
        s2 = "".join(i+" " for i in s2)
        r1 = requests.post('http://localhost:5005/model/parse', json={"text": s1})
        r2 = requests.post('http://localhost:5005/model/parse', json={"text": s2})
        response1 = r1.json()
        response2 = r2.json()
        confsum = response1["intent"]["confidence"] + response2["intent"]["confidence"]
        if confsum > confidence_sum:
            confidence_sum = confsum
            sentence1 = s1
            sentence2 = s2
            resp1 = response1
            resp2 = response2
    return sentence1, sentence2, resp1, resp2

def SplitInput(text):
    text_as_list = re.sub("[^\w]", " ",  text).split()
    #conflist = []
    intent_list = []
    print(len(text_as_list), text)
    #print(text, len(text_as_list), print(text_as_list))
    if len(text_as_list)>2:
        for word_idx in range(len(text_as_list)-1):
            s1 = text_as_list[:word_idx+1]
            s1 = "".join(i+" " for i in s1)
            s2 = text_as_list[word_idx+1:]
            s2 = "".join(i+" " for i in s2)
            r1 = requests.post('http://localhost:5005/model/parse', json={"text": s1})
            response1 = r1.json()
            name = response1["intent"]["name"]
            if response1["intent"]["confidence"]>0.7:
                if name not in intent_list:
                    intent_list.append(name)
                #print(s2)
                SplitInput(s2)
            #print(s1)
            #print(s2)
            #print("----------")
        #conflist.append(response1["intent"]["confidence"])
    #print(intent_list)

if __name__ == "__main__":
    path_to_file = "scenarios.json"
    test_input = "tyvärr så ingår inte rabatt till hörlurarna men om du är"# intresserad så kan du bli telia life kund där du får möjlighet att få många olika förmåner som mer surf och allt möjligt"
    SplitInput(test_input)

    op = 4
    if op == 0:
        test_input = "visst kan jag fixa rabatt till dig och om du blir telia life kund där du får möjlighet att få många olika förmåner som mer surf och allt möjligt"
        r = requests.post('http://localhost:5005/model/parse', json={"text": test_input})
        response = r.json()
        #print(response)
        PrintIntentRanking(response)

        s1,s2,r1,r2 = SplitInputIntoTwo(test_input)
        print(s1)
        PrintIntentRanking(r1)
        print(s2)
        PrintIntentRanking(r2)
    
    if op == 1:
        input_list = test_input.split(" ")
        for it in range(1,len(input_list)):
            s = input_list[:it]
            sentence = "".join(i+" " for i in s)
            print("--------------------------------")
            print(sentence)
            r = requests.post('http://localhost:5005/model/parse', json={"text": sentence})
            response = r.json()
            #print(response)
            PrintIntentRanking(response)
    
    if op == 2:
        responses = GetResponsesFromFile(path_to_file)
        PrintData(responses, 2, 0.5)
        print("Mean confidence: ", MeanConfidence(responses))