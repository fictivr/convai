import tensorflow as tf
import tensorflow_addons as tfa
import sys, pickle
from train_model import KerasModel
from utils.layers import SparseDropout, DenseForSparse, Embed
from utils.transformer import TransformerEncoder
from split_input import SplitInput
from pickle_featurizer import SparseFeaturizer, GetData, TextPreproccessing
from utils.constants import (
    THRESHOLD,
    )

def ExactMatch(model, x, x_tens, y, verbose=True):
    # This function is checking the exact match of predicted vs true, the stricted multi-label classification metric
    # If verbose is True it is printing out the incorrect classifications.
    pred = model.predict(x_tens) > THRESHOLD
    true = y > THRESHOLD
    correct = 0
    incorrect = 0
    for idx in range(len(y)):
        sent_len = len(x[idx])
        sent_pred = pred[idx][:sent_len*2]
        sent_true = true[idx][:sent_len*2]
        if (sent_pred == sent_true).all():
            correct += 1
        else:
            incorrect += 1
            if verbose == True:
                print(x[idx])
            ind = 0
            token_list = []
            for _ in range(int(len(sent_pred)/2)):
                binary = sent_pred[ind:ind+2]
                ind += 2
                if binary[0] == False:
                    if binary[1] == False:
                        token_list.append(str(0))
                    else:
                        token_list.append("S")
                else:
                    if binary[1] == False:
                        token_list.append("E")
                    else:
                        token_list.append("C")
            if verbose == True:
                print(token_list)
    score = correct/(correct + incorrect)
    return score

if __name__ == "__main__":
    path_to_NLU = Path(PATH_TO_DATA)
    utterings, labels = GetData(path_to_NLU)

    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(utterings, labels, shuffle=False, test_size = TEST_SIZE)
    feat = SparseFeaturizer(x_train, n=5)
    x_train_tensor, y_train_tensor = TextPreproccessing(feat, x_train,y_train)
    x_test_tensor, y_test_tensor = TextPreproccessing(feat, x_test, y_test)
    
    model = KerasModel(feat, x_train_tensor, y_train_tensor)
    splitter = SplitInput(model, feat)
    loss, precision,recall, binary_acc, hammingloss = model.evaluate(
            x = splitter.SentencesToTensor(x_test), 
            y= y_test_tensor,
            )
    print("Loss: " + str(loss))
    print("Recall: " + str(recall))
    print("Precision: " + str(precision))
    print("Hamming Loss: " + str(hammingloss))
    print("Binary Accuracy: " + str(binary_acc))
    exact_match = ExactMatch(model, x_test,x_test_tensor, y_test, verbose = verbose)
    print("0/1 or Exact Match Score: " + str(exact_match))
    