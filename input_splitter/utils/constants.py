# Constants
# Requirements tensorflow 2.3.0 tensorflow addons 0.11.2
# Python 3.5.2
SOFTMAX = "softmax"
MARGIN = "margin"
INNER = "inner"
COSINE = "cosine"

# Hyper parameters
INPUT_SPARSITY = 0.8
UNITS = 256
TRANSFORMER_LAYERS = 2
MULTI_ATTENTION_HEADS = 4
TRANSFORMER_SPARSITY = 0.8
TRANSFORMER_DROPOUT_RATE = 0.1
REGULARIZATION_LAMBDA = 0.002
EMBEDDING_DIMENSIONS = 20
MAX_WORDS = 29
BATCH_SIZE = 32
EPOCHS = 100

# Set as True for debugging, False otherwise
RUN_EAGERLY = False

# Threshold for classification
THRESHOLD = 0.9

# Train-test
TEST_SIZE = 0.2     # Ratio of data used for testing
N_SPLITS = 10   # Number of groups for K-fold cross validation

# For synthesizing multi-intents
MIN_WORDS = 4   # Minimum word length of intents in synthesized multi-intents 
SYNTHESIZED_SAMPLES = 1500 # Number of synthesized multi-intents

# Paths
PATH_TO_DATA = "data/data.md"