## intent:restart
- börja om
- Börja om
- Starta om
- starta om
- Starta om scenariot
- starta om scenariot
- börja om scenariot
- Börja om scenariot
- restart
- Restart
- omstart
- Omstart   

## intent:evaluate
- Kan du utvärdera er nuvarande partner?
- Vad tycker ni om er nuvarande partner?
- Vad tycker du?
- vad tycker ni om det?
- Vad tycker ni?
- kan du utvärdera det?
- Hur nöjda är ni?
- hur nöjd är ni med er nuvarande partner
- Härligt, Hur tycker du att det fungerar?
- Hur fungerar ditt [bredband](product)?
- Tycker du hastigheten fungerar bra eller skulle vilja ha snabbare?
- hur tycker du det låter
- hur låter det

## intent:inform+q_happy
- Som du nämnde tidigare så har du ett [mobilabonnemang](subscription) hos oss idag
- Känner du dig nöjd med din nuvarande lösning?
- Ser att du har ett [mobilabonnemang](subscription) hos hos, som du nämnde
- Är du nöjd med det?
- Är du nöjd med det [mobilabonnemang](subscription) jag ser du har?
- Som du nämnde tidigare så har du [mobilabonnemang](subscription) oss idag
- Känner du dig nöjd med din nuvarande lösning
- Som du sa tidigare så har du ett [mobilabonnemang](subscription) hos oss idag 
- Känner du nöjd med din nuvarande lösning
- Som du nämnde tidigare så har du ett [mobilabonnemang](subscription) hos oss idag
- känner du dig nöjd med det
- Som du sa tidigare så har du ett [mobilabonnemang](subscription) oss idag
- Känner du dig nöjd med det
- jag ser att du har ett [mobilabonnemang](subscription) hos oss, 
- är du nöjd med det?
- jag ser att du har [bredband](product) hos en annan leverantör
- Är du nöjd med deras [bredbandslösning](product) 

## intent:q_happy
- är ni nöjda
- är ni nöjda med er nuvarande samarbetspartner?
- är ni nöjda?
- Är ni nöjda?
- är ni nöjda med er partner då?
- är du nöjd med den lösningen i dagsläget?
- är du nöjd med det
- är du nöjd med det?
- är du nöjd med ditt [mobilabonnemang](subscription)
- är du nöjd med det [mobilabonnemang](subscription) som tidigare nämndes
- Är du nöjd med din hastighet?
- Är du nöjd att vara kund hos oss?
- är du nöjd med den lösningen?
- Är du nöjd med din [surfmängd](surf)?
- Är du nöjd med ditt mobil [abonnemang](subscription) hos oss?
- Är du nöjd med allt annat?
- Är du nöjd med din nuvarande [tv och bredbandlösning](product)?
- Och du är nöjd med det
- Gillar ni er nuvarande samarbetspartner?
- Skulle ni säga att ni är nöjda med er nuvarande partner?
- fungerar det bra?
- Känner du dig nöjd med din nuvarande lösning
- Känner du dig nöjd med det [abonnemang](subscription) du har?
- Känner du att du har ett stabilt [bredband](product)?
- Känner du att du har tillräckligt med [surf](surf) varje månad?
- Är du nöjd med hur [bredbandet](product) fungerar
- fungerar [bredbandet](product) bra?
- Fungerar [bredbandet](product) bra
- Funkar det bra tycker du?
- Har du bra [surf](surf) på mobilen?
- upplever du att det flyter på bra?
- så du är nöjd med allt?
- upplever du att det fungerar bra med den hastigheten?
- Är du nöjd med din [tv och bredbandslösning](product)?
- Nöjd med [tv](product) utbudet?
- men är du nöjd annars med din nuvarande [bredbandslösning](product)?
- Är du nöjd med hastigheten på [bredbandet](subscription)?
- Hastigheten då, fungerar den bra?
- Har du en jämn hastighet på ditt [bredband](product)?

## intent:deny+q_happy
- Nej tyvärr, Men är du nöjd med din nuvarande mobil [abonnemang](subscription) lösning?
- Nja, Det är svårt för mig att se
- Är du nöjd med din [surf](surf) mängd?
- aj då, det går tyvärr inte 
- Är du annars nöjd med ditt [abonnemang](subscription)?

## intent:inform_sad+q_happy
- Du kan tyvärr inte få någon [rabatt](discount) på hörlurarna
- Är du nöjd med ditt [bredband](product) och [mobilabonnemang](subscription) i dagsläget?
- Jag kan tyvärr inte ge dig [rabatt](discount)
- är du annars nöjd med ditt [abonnemang](subscription)?
- Ingen [rabatt](discount), Är du nöjd annars med ditt [abonnemang](subscription)? 
- nöjd med [surf](surf) mängden och så?
- Jag kan inte ge dig [rabatt](discount)
- Men tycker du att ditt [abonnemang](subscription) duger?
- Som jag ser i min dator kan jag inte ge dig någon [rabatt](discount)
- Fungerar ditt [abonnemang](subscription) bra annars då?
- Det finns ingen [rabatt](discount)
- Men du är nöjd med ditt [abonnemang](subscription)?
- Jag kan inte ge någon [rabatt](discount) men
- jag skulle bara kolla Är du nöjd med ditt [mobilabonnemang](subscription) hos oss

## intent:q_unhappy
- något ni är mindre nöjda med?
- något ni är missnöjd med?
- något ni inte är helt nöjd med?
- något ni inte är helt nöjda med
- något du är mindre nöjd med gällande [tv och bredband](product)
- något som har fungerat mindre bra då?
- Är det något i den nuvarande lösningen som du känner dig missnöjd med
- Är det något i din nuvarande lösning Så du känner dig missnöjd med
- är det något ni är mindre nöjda med?
- Är de något i din nuvarande lösning som du känner dig missnöjd med?
- Är det någonting ni var en lösning som var missnöjd med
- Är det något som du inte är nöjd med
- är det något ni är mindre nöjd med?
- är det något du är mindre nöjd med?
- Är det något in i det nuvarande lösningen som du känner dig missnöjd med
- Är det något i din nuvarande lösning som du känner dig missnöjd med
- Är det något du inte är nöjd med
- Är du missnöjd med något?
- Är du missnöjd med något i ditt [mobilabonnemang](subscription)?
- finns det något ni inte är helt nöjda med?
- finns det något ni inte är nöjda med
- finns det något du inte är nöjd med
- finns det något du inte är helt nöjd med
- något som du är mindre nöjd med?
- har du haft problem med ditt [fiber](product)?
- så om jag förstår det rätt så har du haft problem

## intent:ill_help+q_unhappy
- Självklart, det ska jag ordna
- Är det något i din nuvarande lösning som du känner dig missnöjd med?
- det ska jag kunna hjälpa dig med
- är du missnöjd med något i din nuvarande lösning
- det ska jag givitvis kunna hjälpa dig med
- är det något du är mindre nöjd med i din nuvarande lösning

## intent:q_why_unhappy
- Vad är du inte nöjd med?
- vad är ni mindre nöjd med då?
- vad krävs för en tia?
- varför inte en tia?
- vad saknas för en tia
- vad roligt att ni är nöjda
- vad skulle krävas för att det skulle vara en tia?
- vad roligt att ni är nöjda, 
- vad saknas för att det skulle vara en tia?
- kul att ni är nöjda
- vad krävs för en tia?
- vad skulle krävas för ett högre betyg?
- vad är det för problem du har upplevt?
- vad är det som inte fungerar

## intent:gives_price+q_was_that_all
- dom kostar [400 kronor](number)
- var det bra så?
- toppen,dom kostar [499 kronor](number)
- var det bra så?
- prisvärda är dom det blir [700kr](number) 
- var det bra så
- ok, dom kostar [999 kronor](number)
- var det bra så?

## intent:q_was_that_all
- Var det bra så?
- är du nöjd så ?
- var det allt?
- var det allt
- är du nöjd så med [hörlurarna](product)?
- är du nöjd med att endast köpa [hörlurarna](product)?
- var det allt du ville ha?

## intent:q_anything_else
- behöver du något mer?
- vill du ha något mer?
- ville du ha något mer?
- Något förutom det?
- Något utöver det?
- utöver det?
- något utöver det?
- något annat?
- något annat än det?
- Något annat?
- något annat du vill ha?
- Något annat du behöver?
- Finns det något annat du vill ha?
- saknar du någonting i din nuvarande lösning
- saknar du något?
- något du saknar i den lösningen då?
- Vad bra, finns det något som du saknar i din nuvarande lösning?
- Vad bra Är det någon som saknar din nuvarande lösning
- Vad bra Finns det någon som du saknar i din nuvarande lösning
- Vad bra Finns det något du något som du saknar din nuvarande lösning
- Vad bra Finns det något som du saknar din nuvarande lösning
- Vad bra Finns det något som du saknar i din nuvarande lösning
- finns det något du saknar i lösningen
- Är det något du saknar gällande [tv och bredband](product)?
- Känner du att du saknar något just nu i ditt [bredband eller tv](product) utbud?
- Känner du att du saknar något i ditt [mobilabonnemang](subscription)?
- Känner du att det saknas något gällande [tv eller bredbandet](product)?
- Saknas det något gällande [tv och bredbandet](product)?
- Finns det något som skulle kunna göra dig mer nöjd med ditt [bredband](product)?
- tycker du [abonnemanget](subscription) fungerar bra 
- eller finns det något som du saknar?
- är du nöjd med [abonnemanget](subscription) hos oss 
- eller finns det något du upplever att du saknar?
- finns det något som skulle kunna göra dig mer nöjd med din nuvarande lösning
- finns det något som skulle göra dig ännu mer nöjd
- inte det, så du saknar ingenting i din nuvarande lösning
- något mer du skulle vilja ha i din lösning
- något mer du skulle vilja ha i din nuvarande lösning
- Finns det något annat som jag ska ta hänsyn till innan jag tar fram en lösning
- Finns det något annat jag ska ta hänsyn till innan jag tar fram en lösning?
- Finns det något annat jag ska ta hänsyn till innan jag tar fram en lösning
- Finns det något annat som jag skulle ta hänsyn till innan jag ta fram en lösning
- Finns de något annat jag ska ta hänsyn till innan jag presenterar en lösning?
- finns det något annat jag ska ta hänsyn till?
- något annat jag skulle ta hänsyn till innan jag tar fram en lösning
- något annat jag ska ta hänsyn till innan jag tar fram en lösning
- något annat jag ska ta hänsyn till innan jag tar fram en lösning?
- något annat jag ska ta hänsyn till
- Finns det något annat jag kan göra för dig innan jag tar fram en lösning?
- Finns det något som skulle kunna förbättra din upplevelse?
- behöver du hjälp med något annat?
- ingenting annat?
- Ingenting annat
- Inget jag kan hjälpa dig med?
- Vill du ha hjälp med något mer?
- Ville du ha hjälp med något mer?
- Okej finns det något som ni skulle behöva ha hjälp med?
- något annat du är intresserad av
- något annat ni är intresserad av
- finns det något annat du är intresserad av
- finns det något annat ni är intresserade av
- Har du några andra tjänster som du är intresserad av ?
- har du något annat som du är intresserad av
- har ni något annat som ni är intresserade av
- några andra tjänster som du är intresserad av?
- några andra tjänster som ni är intresserade av
- finns det några andra tjänster som du är intresserad av?
- finns det några andra tjänster som ni är intresserade av
- Någon annan anledning till att inte kunna ta ett beslut idag?
- Finns det något mer som hindrar er från att ta ett beslut nu?
- något förutom det som hindrar er?
- Något mer som hindrar er?
- Något annat som hindrar dig?
- något utöver det som hindrar er just nu?
- något utöver det som hindrar er från att ta ett beslut nu?
- Finns det något som hindrar er från att göra en affär nu?
- något utöver det som hindrar er ifrån att ta ett beslut nu

## intent:q_anything_else+q_was_that_all
- Vill ha hjälp med något mer 
- eller är du nöjd så?
- Något annat 
- eller är du nöjd så?
- finns det något mer jag kan hjälpa dig med 
- eller är du nöjd med [hörlurarna](product)?
- var det något mer 
- eller är det bra så?
- är du nöjd så 
- eller kan jag hjälpa dig med något annat?

## intent:ill_help+q_anything_else
- Självklart ordnar jag det 
- något mer som jag ska titta på?
- självklart så fixar jag det
- vill du ha hjälp med något mer?
- det fixar jag
- något annat du behöver hjälp med?

## intent:q_how_do_I_help_you
- vad kan jag hjälpa dig med?
- vad behöver du hjälp med
- Vad mer önskar du
- Vad saknar du idag?
- vad mer önskar du då?
- vad mer kan jag hjälpa dig med?
- Hur kan jag hjälpa dig då?
- vad kan jag göra för dig
- vad ska jag göra

## intent:q_how_much_r_u_paying_today
- vad betalar du idag?
- vad betalar du idag
- vad har du för månadskostnad idag
- vad har du för månadskostnad idag?
- vad är månadskostnaden idag?
- vad har du för månadskostnad
- vad betalar du i dagsläget
- vad betalar du i dagsläget?
- Vad är din månads kostnad för ditt [mobilabonnemang](subscription)?
- Vad är månadskostnaden för ditt [abonnemang](subscription)
- Vad är månadskostnaden för ditt [abonnemang](subscription) i dagsläget
- Vad är månadskostnaden för ditt [abonnemang](subscription) idag
- vad har du för månadskostnad för ditt [mobilabonnemang](subscription)?
- vad har du för månadskostnad för ditt [mobilabonnemang](subscription) idag 
- Okej, Vad betalar du i månaden?
- Vad betalar du i månaden idag?
- Hur mycket betalar du i nuläget?
- hur mycket betalar du i dagsläget
- Hur mycket betalar du idag?
- Vad kostar ditt [bredband](product) i dagsläget
- vad kostar ditt [bredband](product) idag
- Så vad betalar du i månaden idag?
- så vad betalar du i månaden i dagsläget för ditt [abonnemang](subscription)?
- så vad betalar du i månaden i nuläget för ditt [abonnemang](subscription)?
- så vad betalar du idag för ditt [abonnemang](subscription)?
- så vad betalar du i dagsläget för ditt [abonnemang](subscription)?
- så vad betalar du i nuläget för ditt [abonnemang](subscription)?

## intent:q_do_you_want
- vill du ha?
- Vill du bli [telia life](service) kund
- Vill du ha en [telefon](product) också
- Vill du köpa [hörlurarna](product)?
- Vill du köpa en [dator](product)?
- Vill du ha en [dator](product) också?
- Vill du ha flera [kanaler](channels)?
- Vill du ha [rabatt](discount)?
- vill du inte ha någon [rabatt](discount)?
- Skulle du vilja bli [Telia Life](service) kund?
- Skulle du vilja bli [Telia Life](service) kund
- Ska du ha [hörlurana](product)
- Skulle du ha [hörlurarna](product)
- skulle du vilja ha mer [surf](surf) i mobilen?
- skulle du vilja ha mer [kanaler](channels)?
- skulle du vilja ha fler fotbolls [kanaler](channels)?
- så ska du ha [telia life](service)
- du vill inte ha en ny [telefon](product)?
- du vill inte ha en [försäkring](insurance) till dom också?
- okej, men vill du köpa till en [försäkring](insurance) till dina hörlurar?
- Skulle du ha [hörlurarna](product) eller inte?
- Ska du ha [hörlurarna](product) eller inte?
- är intresserad av fler [kanaler](channels) kanske?
- är du intresserad av [telia life](service)?
- är du intresserad av en ny [dator](product)?
- är du intresserad av en ny [telefon](product)
- upplever du att du behöver mer [surf](surf) till mobilen?
- du vill ha lite mer [surf](surf) kanske?
- men lite mer [surf](product) kanske?
- behöver du fler [kanaler](channels)?
- nehe, men du sa att du ville ha mer [surf](surf)?
- kanske en [mobil](product) också
- Vill du se allsvenskan på tv?
- allsvenskan kanske?
- Vill du köpa till en påse?
- vill du ha en påse till det?
- behöver du en påse till hörlurarna?
- jag fixar mer [surf](surf) till dig
- då kan vi lägga till mer [surf](surf) till ditt abonnemang
- då lägger jag till mer [surf](surf) till ditt abonnemang
- okej, ska vi lägga på [100](number) gig [surf](surf) varje månad?
- okej men då kan vi lägga till lite mer [surf](surf) på ditt paket
- du måste bli [telia life](service) kund
- då fixar vi dom bästa [sportkanalerna](channels) till dig så att du får njuta ordentligt
- jag fixar dom bästa [fotbollkanalerna](channels), samt [hörlurarna](product)
- skulle du vilja skaffa [bredband](product) hos oss
- skulle du vilja ha ett bättre [abonnemang](subscription)?

## intent:q_why_happy
- Vad är ni nöjd med
- vad är du nöjd med
- Vad är fördelen med den partner som ni har idag?
- vad är fördelen med partnern ni har idag?
- vad är fördelen med det samarbetet ni har idag?
- vaf är fördelen med det samarbetet ni har i dagsläget
- Vad är det som är så bra med er partner idag?
- vad är det som är så bra med den partnern ni har idag
- vad är det som är bra med den partner ni har idag?
- varför är ni så nöjda?
- varför är du så nöjd?
- varför är du nöjd
- varför är ni nöjda
- kan du säga varför ni är nöjda
- kan du säga varför du är nöjd
- kan du säga varför ni är så nöjda
- kan du säga varför du är så nöjd
- Vad är du mest nöjd med i den lösningen?
- vad känner du dig mest nöjd med i den lösningen
- vad är det som gör att du känner dig nöjd
- vad är det som gör att du känner dig så nöjd
- i din nuvarande lösningen som du har idag vad känner du dig nöjd med?
- i din nuvarande lösning som du har idag Vad känner du dig nöjd med
- i din nuvarande lösning som du har idag, vad är det som gör att känner du dig nöjd?
- i den lösning som du har idag Vad känner du är nöjd med
- i den lösningen som du har Vad känner du dig extra nöjd med
- i den lösningen som du har idag Vad känner du dig nöjd med

## intent:ill_help+q_why_happy
- Självklart, det ska jag givetvis hjälpa dig med
- I din nuvarande lösningen vad känner du dig nöjd med?
- Det ska vi kunna hjälpa dig med, 
- vad känner du dig nöjd med?
- det ska jag kunna fixa till dig
- vad känner du dig nöjd med i din nuvarande lösning

## intent:affirm
- Ja
- Ja det kan jag erbjuda
- Ja det ser ut att stämma
- Ja men det är toppen
- Ja men visst
- ja självklart
- Ja du har fått det
- Okej
- okej
- Okej det var bra
- Okej som jag förstår så
- absolut
- precis
- låter bra
- korrekt
- japp, bara för dig
- Jaha
- jaha
- ja det stämmer
- Gör det!
- Gör så
- ok bra
- visst
- det går bra
- Okej det kör vi på!
- Sure
- Hej det går bra
- hej absolut
- hej okej

## intent:deny
- nej
- aldrig
- tror inte det
- gillar det inte
- absolut inte
- inte direkt
- Det är fel
- det är felmärkt
- det är inte korrekt
- Nej det är det inte
- Nej det har du inte
- Nej, tyvärr
- Det går inte
- nej, inte den här gången

## intent:q_product_usage
- hur mycket [surf](surf) brukar du använda?
- Hur mycket [surfar](surf) du?
- Hur mycket [surf](surf) brukar du använda en månad
- hur mycket [surf](surf) brukar du använda per månad
- Till vad använder du [headsetet](product)
- Till vad använder du [hörlurarna](product)
- Vad använder du ditt [bredbandet](product) mest till när det fungerar dåligt?
- vad använder du [internet](product) mest till när du upplever problemen?
- vad använder du ditt [internet](product) till?
- vad brukar ni använda [surf](surf) till?
- Vad brukar du använda [surfen](surf) till?
- Vad brukar ni använda [internet](product) till
- Vad brukar du använda [internet](product) till
- Vad brukar du använda ditt [internet](product) till?
- Vad brukar du använda ditt [internet](product) till
- Vad brukar du göra på [internet](product) då?
- hur använder du [internet](product)?
- Hur använder du din [telefon](product)?
- Hur Brukar du använda [internet](product)
- något speciellt du gör som tar mycket [surf](surf)?
- Är det något speciellt du gör på [mobilen](product) som tar mycket [surf](surf)?
- Är det något speciellt du gör som tar mycket [surf](surf)?

## intent:q_when
- när
- när då
- när hade det passat?
- när är en bra tid
- när är en bra tid tror du
- När har ni mer tid tror du?
- När har ni mer tid?
- när skulle det passa?
- när har ni tid
- när hade det varit en bra timing
- när har ni mer tid?
- när kan du träffas?
- när skulle ni säga att ni har mer tid?
- När tror du det lugnat ner sig något?
- När hade varit mer passande?
- När hade det varit en bättre tidpunkt?
- När i månaden brukar det ta slut?
- När brukar det ta slut i månaden?
- Vad hade varit en passande tidpunkt?
- Timing är viktigt, när har ni mer tid tror du?
- Timing är viktigt, när hade det passat?
- Timing är viktigt, när skulle det passa er?
- Timing är viktigt, när skulle det passa dig bättre
- Timing är viktigt, vad hade varit en bra timing
- Timing är viktigt, när har ni mer tid
- Timing är viktigt, vad hade varit en bra timing
- Timing är viktigt, när skulle du säga ni har mer tid
- Timing är viktigt, när tror du det har lugnat sig något
- Timing är viktigt, när är en bra tid
- Timing är viktigt, när har ni tid
- Timing är viktigt, vad är en bra tid för er
- Timing är viktigt, vad är en bra tid för dig
- Timing är viktigt, när skulle det passa
- Timing är viktigt, när har det lugnat sig något
- Timingen är självklart något vi behöver ta hänsyn till, När hade varit en passande tidpunkt?
- Timingen är självklart något vi behöver ta hänsyn till, när har ni mer tid tror du?
- Timingen är självklart något vi behöver ta hänsyn till, när hade det passat?
- Timingen är självklart något vi behöver ta hänsyn till, när skulle det passa er?
- Timingen är självklart något vi behöver ta hänsyn till, när skulle det passa dig bättre
- Timingen är självklart något vi behöver ta hänsyn till, vad hade varit en bra timing
- Timingen är självklart något vi behöver ta hänsyn till, när har ni mer tid
- Timingen är självklart något vi behöver ta hänsyn till, vad hade varit en bra timing
- Timingen är självklart något vi behöver ta hänsyn till, när skulle du säga ni har mer tid
- Timingen är självklart något vi behöver ta hänsyn till, när tror du det har lugnat sig något
- Timingen är självklart något vi behöver ta hänsyn till, när är en bra tid
- Timingen är självklart något vi behöver ta hänsyn till, när har ni tid
- Timingen är självklart något vi behöver ta hänsyn till, vad är en bra tid för er
- Timingen är självklart något vi behöver ta hänsyn till, vad är en bra tid för dig
- Timingen är självklart något vi behöver ta hänsyn till, när skulle det passa
- Timingen är självklart något vi behöver ta hänsyn till, när har det lugnat sig något

## intent:summerize
- Okej förstår det rätt så har du till och från har problem med hastigheten hemma 
- utöver det så har du behövt mer [surf](surf)
- och är intresserad att kunna se Allsvenska Stämmer det
- Okej så som jag förstår så har det fungerat bättre efter du har skaffat fiber
- och du är intresserad av fotboll
- och du vill ha [hörlurarna](subscription), stämmer det?
- Jag förstår, Om jag har förstått dig rätt så vill du köpa [hörlurarna](product)
- samt ha lite mer [surf](surf), stämmer det?
- Okej, så om jag förstår dig rätt så har du till och från haft problem med hastigheten hemma
- Utöver det så hade du behövt mer [surf](surf) i ditt [mobilabonnemang](subscription)
- och är intresserad av att kunna se allsvenska stämmer det?
- Okej som jag förstår rätt så har du till och haft till och från haft problem med hastigheten hemma 
- utöver det så hade du behövt mer [surf](surf) i mobilen
- och var inte jag är intresserad av att kunna se Allsvenskan Stämmer det
- Så om jag förstår dig rätt så skulle du vilja ha mer [surf](surf) i mobilen, [sportkanaler](channels) med fotboll framförallt allsvenskan 
- och se över varför du har problem med hastigheten ibland
- Okej, om jag förstår dig rätt så vill du ha flera [sportkanaler](channels) med fotboll - samt mer [surf](surf) i mobilen och [hörlurarna](product) stämmer det?

## intent:q_many_users
- är ni flera i hushållet som använder [internet](product)?
- Är ni flera som använder [internet](product)?
- Är ni många som använder [internet](product)?
- Är ni flera i hushållet som använder [internet](product)
- är ni många hemma som använder [internet](product)?
- Är ni fler än du i hushållet som använder samma [internet](product)
- är ni flera i hushållet som använder [internet](product)
- är ni många i hushållet som använder [internet](product)?
- Är ni flera i hushållet som har [mobiltelefoner](product)?

## intent:q_live_alone
- Är du ensamstående?
- Så du bor själv eller?
- bor du själv?

## intent:q_using_product_alone
- Är det bara du som använder [bredbandet](product)?
- bara du hemma som använder [internet](product)

## intent:q_how_many_users
- Hur många använder [internet](product)?
- Bor du själv eller delar du [internet](product) med andra?
- är du ensam som använder [fiber](product) eller är ni flera som delar?
- är det bara du eller är ni flera som använder [bredbandet](product) hemma?

## intent:promoting_himself
- Jag är duktig på att lyssna
- jag är en bra lyssnare
- jag jobbar hårt och mycket
- jag jobbar hårt för att ge mina kunder det de vill ha
- Jag kommer alltid att vilja förstå din verksamhet på djupet för att ge de bästa råden
- jag kan ge er mycket bättre service
- jag kan ge er bättre service och kostnadskontroll
- jag kan visa att jag kan ge er bättre service
- jag kan ge dig bra förmåner

## intent:promoting_company
- Vi kommer kunna hjälpa er att effektivisera er verksamhet genom att förbättra processer och flöden, samtidigt som det bidrar till ökad flexibilitet
- Genom en gedigen behovsanalys säkerställer vi era viktigaste målsättningar och prioriterar de största utmaningarna som hindrar er att nå dit
- Vi kommer fokuserar på att vara din tekniska rådgivare, säkerställa rätt val för dig som kund och alltid göra det lilla extra
- Genom att reducera antalet modeller för ett företag skapade vi snabbare leverans, bättre inköp och förbättrad service
- vi kan ge bättre kontroll av kostnader
- vi kan ge er ökad kontroll över era kostnader
- vi kan ge er bättre service
- vi kan ge utökad service
- vi skulle kunna fixa bättre service till er
- Vi kan både ge bättre kostnadskontroll och service
- jag kan visa er att vi kan ge utökad service
- om jag kan visa att jag kan ge er bättre service
- Om jag skulle kunna visa att vi både kan ge er ökad service och total kostnadskontroll
- Om du samlar dina fasta och mobila tjänster hos oss kan du få olika förmåner
- Sen har jag kollat på vår täckningskarta och du ska ha full 4G-täckning med [Telia](company)

## intent:promoting_company+expects_agreement
- Sen har jag kollat på vår täckningskarta och du ska ha full 4G täckning med Telia
- Det skulle passa va?
- vi kan ge er bättre service
- vilket låter bra hoppas jag?

## intent:promoting_product
- det innebär att när du samlar dina fasta och mobila tjänster hos oss så får du välja bland olika förmåner Exempelvis mer [surf](surf) i mobilen
- det skulle innebära att du kan få mer [surf](surf) i mobilen
- om du blir [telia life](service) kund kan du få välja mellan olika förmåner
- vi har en tjänst som kan ge dig mer [surf](surf) i mobilen
- Det innebär att du får välja bland alla våra förmåner, till exempel mer [surf](surf) till [mobilen](product)
- då kan du samla dina fasta och mobila tjänster hos oss och så får du välja mellan olika förmåner, till exempel mer [surf](surf) i [mobilen](product)
- det skulle ge dig chansen att välja mellan olika förmåner, till exempel mer [surf](surf) i mobilen
- genom att samla dina fasta tjänster och mobila tjänster så får du välja bland massa förmåner Som ett exempel skulle du kunna få mycket mer [surf](surf) i mobilen
- Du skulle kunna skaffa snabbare [fiber](product) hastighet så du kan ladda ner saker snabbare och streama videos otroligt fort
- det gör att du kan samla din fasta och mobila tjänster hos oss och genom det får du massa förmåner
- du får du välja mellan olika förmåner, till exempel mer [surf](surf)
- du samlar dina mobila och fasta tjänster hos oss vilket kan ge dig förmåner, som mer [surf](surf)
- Då kan du samla dina fasta och mobila tjänster hos oss och sen får du välja bland massa förmåner
- Då kan du samla dina fasta och mobila tjänster och få förmåner som till exempel mer [surf](surf)
- Va bra, vi har en tjänst som heter [Touch Point](service) som gör de möjligt för alla dina anställa att kunna svara på inkommande samtal
- Utifrån det du har berättat för mig så skulle jag rekommendera [bredband](product) 100/100 med tv-paket lagom och Dplay
- Utifrån det du har berättat för mig så skulle jag rekommendera [bredband](product) 100/100 med tv-paket lagom och Dplay 
- Då har du tillgång till de befintliga kanalerna som du har idag samt all fotboll som sänts via allsvenskan
- Att bli [Telia life kund](service) innebär att du får välja bland våra förmåner
- Då ingår det totalt 24 tv-kanaler och all fotboll som sänts via allsvenskan
- Utöver det så uppdateras ditt [mobilabonnemang](subscription) så du får mer [surf](surf) i mobilen från om med idag
- vilket innebär att du får välja bland våra förmåner
- Utifrån vad du berättade så hade mer [surf](surf) till ditt [mobilabonnemang](subscription) utan någon extra kostnad kunnat vara något för dig
- Till [telefonen](product) så rekommenderar jag [abonnemanget](subscription) som vi kallar Obegränsat med en extra användare
- där både du och din dotter skulle få fria samtal, sms, mms och obegränsat [surf](surf)
- Detta innebär att du slipper fylla på hennes kontantkort och få oväntade kostnader
- Ännu en fördel med att vara [Telia Life-kund](service) är att du kan byta din förmån när som helst om du skulle vilja ha något annat till hösten eller vintern
- Då ingår det totalt 4 [kanaler](channels) och allt fotboll som sänds via allsvenskan
- utöver det så uppdateras ditt [mobilabonnemang](subscription) med [telefon](product) med [surf](surf) i mobilen från och med idag
- Då ingår det totalt 4 [kanaler](channels) och allt fotboll som sänds via allsvenskan
- Utöver det så uppdateras ditt [mobilabonnemang](subscription) med [telefon](product) med [surf](surf) i mobilen från och med idag
- om du skulle skaffa [bredband](product) hos oss skulle du kunna få massa förmåner
- du kan bli [telia life](service) kund där du får möjlighet att få många olika förmåner som mer [surf](surf) och allt möjligtx

## intent:deny+promoting_product
- nej inte den här gången
- men du kan bli [telia life](service) kund och få massa med förmåner
- nej tyvärr
- men du kan få en massa förmåner om du blir [telia life](service) hos oss

## intent:inform
- Ser att du har ett [mobilabonnemang](subscription) hos hos, som du nämnde
- Som du nämnde tidigare så har du ett [mobilabonnemang](subscription) hos oss idag
- Jag ser här att du har möjlighet att bli [Telia Life-kund](service)
- okej, jag ser här att du har möjlighet att bli [telia life](service) kund
- du kan bli [telia life](service) kund
- Eftersom du redan har ett [mobilabonnemang](subscription) hos oss idag så kan du bli en [Telia Life](service) kund
- jag ser här att du har möjlighet att bli [telia life](service) kund hos oss
- du har möjlighet att bli [Telia Life](service) Kund om du har ett [mobilabonnemang](subscription) hos oss
- du har möjlighet att bli [Telia Life](service) kund hos oss
- du har möjlighet att bli [telia life](service) kund 
- Jag ser att du har möjlighet att bli [Telia Life](service) kund
- Men jag ser att du har möjlighet att bli [Telia Life](service) kund
- Det jag ser här att du har möjlighet att bli [Telia Life](service) kund
- Eftersom du redan har ditt [mobilabonnemang](subscription) hos oss idag så blir du automatiskt en [Telia Life-kund](service)
- Eftersom du har ditt [mobilabonnemang](subscription) hos oss så blir du automatiskt en [Telia Life-kund](service) i och med detta
- det ger i sin tur dig möjligheten att bli [telia life](service) kund

## intent:expects_agreement
- eller hur?
- vilket var något du var intresserad av, eller hur
- vilket var något som skulle underlätta för er verksamhet eller hur?
- det låter bra, eller hur?
- smidigt eller hur?
- det var något du var intresserad av eller hur
- Om jag förstod dig rätt så var det så du ville ha det?
- stämmer det?
- det skulle passa va?
- det låter väl bra?
- det stämmer väl?
- låter det bra?
- vad bra, ska vi säga så
- ska vi säga så
- ska vi säga så?

## intent:promoting_product+expects_agreement
- Utifrån det du har berättat för mig så skulle jag rekommendera [bredband](product) 100/100 med tv-paket lagom och Dplay
- Då har du tillgång till de befintliga kanalerna som du har idag samt all fotboll som sänts via allsvenskan 
- vilket var något som du var intresserad av
- eller hur?
- Det innebär att du får välja bland våra förmåner
- Utifrån vad du berättade så hade mer [surf](surf) till ditt [mobilabonnemang](subscription) utan någon extra kostnad kunnat vara något för dig
- stämmer det?
- Vi har en tjänst som heter [Touch Point](service) som gör de möjligt för alla dina anställa att kunna svara på inkommande samtal
- vilket var något som skulle underlätta för er verksamhet eller hur?
- Ännu en fördel med att vara [Telia Life-kund](service) är att du kan byta din förmån när som helst om du skulle vilja ha något annat till hösten eller vintern
- smidigt eller hur?
- Då ingår det totalt 4 [kanaler](channels) och allt fotboll som sänds via allsvenskan
- utöver det så uppdateras ditt [mobilabonnemang](subscription) med [telefon](product) med [surf](surf) i mobilen från och med idag 
- hur låter det?
- Då ingår det totalt 4 [kanaler](channels) och allt fotboll som sänds via allsvenskan
- det låter väl bra?
- Utöver det så uppdateras ditt [mobilabonnemang](subscription) med [telefon](product) med [surf](surf) i mobilen från och med idag
- vilket var något du var intresserad av eller hur

## intent:q_why_am_I_here
- Jaha varför är jag här då?
- Vad gör jag här?
- Varför har vi detta möte?
- vad gör vi här egentligen?
- okej, så vad gör jag här?
- behöver ni hjälp med något? 
- eller varför har vi bokat det här mötet?
- varför har vi bokat detta möte
- jaha varför är jag här då?
- varför är jag här?
- vad gör jag här då?
- okej, vad gör jag här då?
- okej vad gör jag här då

## intent:pleasantry
- du får ha en fortsatt trevlig dag
- ha en trevlig kväll
- trevlig helg!
- hoppas du får en trevlig sommar
- god fortsättning
- glad påsk
- god jul
- trevlig midsommar
- ha det trevlig dag
- ha det bra
- då får du ha en trevlig dag!
- Då får du ha en trevlig kväll och förlåt än en gång
- Du får ha det så bra!
- Ha en trevlig kväll
- just precis, dom är dina nu
- Ha en bra dag
- ha en bra dag
- jag förstår, Då säger vi så
- Ha en trevlig dag
- Okej, Du vinner, Ha en trevlig dag
- Tack hej och ha en bra dag
- Jag tackar för allt, 
- Hej då och ha en trevlig dag
- Inga problem, det är redan fixat
- Nu får du ha en fantastisk dag
- Aijt, Då önskar jag dig en bra dag
- då önskar jag dig en bra kväll
- då önskar jag dig en forsatt bra dag

## intent:q_whats_next
- Okej så hur går vi vidare?
- så hur går vi vidare
- hur går vi vidare
- hur går vi vidare härifrån
- vad är nästa steg?
- Hur gör vi nu?
- vad gör vi härifrån?
- ja men då så, hur går vi vidare?
- ja men då så, hur går vi vidare

## intent:q_where
- var?
- var
- vart ligger det?
- vart
- vart då?
- var då?

## intent:that_was_all
- det var allt
- har inget mer om det
- det var allt jag hade om [telia life](service)
- har inget mera
- det var allt, låter det bra?
- hade inte så mycket mer om det
- Det var allt

## intent:be_right_back
- ett ögonblick
- vänta här en stund
- strax tillbaka
- kommer tillbaka alldeles strax
- kommer strax tillbaka
- ses om en liten stund
- då ses vi om en liten stund
- du ses vi om en stund
- då ska jag se vad jag kan fixa till dig
- jag kommer tillbaka strax
- Du ses vi om en stund!
- då ses vi om en stund 
- ska jag se om jag kan ta fram en bra lösning till dig
- om du väntar här en stund 
- så ska jag kolla om jag kan ta fram en bra lösning till dig

## intent:ill_be_back
- då kommer jag tillbaka vid ett senare tillfälle 
- då kommer jag tillbaka efter sommaren

## intent:gives_price
- [400](number)
- det blir [499kr](number)
- absolut! Det blir [200](number)
- Det blir [299kr](number)
- [200kr](number)
- det blir [200kr](number)
- det blir [700kr](number)
- Absolut! Det kostar [398](number)
- Ja, det blir [399kr](number)
- Okej, det kostar [299 kronor](number)
- det kostar [400kr](number)
- Det kostar [53kr](number)
- mitt lägsta pris är [501kr](number)
- jag kan inte gå lägre än [152 spänn](number)
- Hej Det kostar [500](number) kr
- Hej Det kostar [500 kr](number)
- de kostar [500 kronor](number)
- hej det kostar [500](number) kr
- Okej Det kostar [500](number) kr
- Det kostar [500kr](number)
- det kostar [500](number) kr
- hej det kostar [500kr](number)
- det kostar [500kr](number)
- då blir det [509,50kr](number)
- [520](number)
- [550](number)
- [595](number)
- [500kr](number)
- [931 kronor](number)
- Då blir det [990kr](number) totalt
- [850kr](number)
- [1000kr](number)
- ok de kostar [500](number) kr
- det blir [499](number) kr
- hej det blir [500 kronor](number)
- det blir [847kr](number)
- det blir [259kr](number)
- det blir [500](number) kr
- det blir [500kr](number)
- det blir [250kr](number)
- Okej det blir [700](number) kr tack
- De kostar [700](number) kr
- absolut det blir [500kr](number)
- inga problem, dom kostar [499 kronor](number)
- Dom kostar [500kr](number) plus moms
- dom ligger på [499 kronor](number)
- Ett bra val, Det blir [399 kronor](number)
- Hej! dom är på Rea just nu och kostar [499 kronor](number)
- du får [250 kronor](number) för dom på grund av mitt beteende
- [Hörlurarna](product) är toppen bra, Dom kostar [659 kronor](number)
- [Hörlurar](product), ah dom där är nya hörlurar
- Det blir [50 kronor](number)
- [Hörlurarna](product) kostar [499 kronor](number)
- du får [hörlurarna](product) för [299 kronor](number)
- Okej men då säger vi [499](number)
- Ja men då är det [599 kr](number) som gäller
- jaha, men då blir det [550kr](number) för [hörlurarna](product)
- Ok, du får de för [499](number)
- eller vi kan säga [509kr](number)
- det blir [250kr](number)
- Totalkostnaden för [tv och bredband](product) är [299 kronor](number)
- vad bra totalkostnaden för [tv och bredband](product) blir [500000](number) kr
- [99 kr](number) i månaden
- okej, vi har ett [abonnemang](subscription) som kostar [349kr](number)

## intent:gives_price+q_anything_else
- [520](number)
- ville du ha något mer?
- Hej Det kostar [500](number) kr 
- Något förutom det?
- Absolut! Det kostar [398](number)
- Något utöver det?
- det blir [700kr](number)
- utöver det?
- ok de kostar [500](number) kr 
- något utöver det?
- nice bra val det blir [53kr](number) 
- något annat?
- prisvärt Då blir det [990kr](number) totalt
- var det något mer
- [Hörlurarna] går på [399 kronor](number)
- Var det något mer?
- Javisst ja, Du har helt rätt, Jag ber om ursäkt
- Var det något mer?
- bra val, dom har otroligt bra ljud dom där, dom kostar [499 kronor](number)
- Var det någonting mer jag kan hjälpa dig med?
- Jasså, Ja, du har helt rätt, Dom kostar [499 kronor](number)
- Kan jag hjälpa dig med något annat
- [Hörlurarna](product) kostar [499 kronor](number)
- Vill du att jag hjälper dig med något annat?

## intent:gives_price+q_customer_today
- Det blir [123kr](number)
- har du några tjänster hos oss idag?
- Självklart, det blir [3410 kronor](number)
- är du kund hos [telia](company) idag?
- Hej Det kostar [500 kr](number)
- Är du kund hos oss idag?
- de kostar [500 kronor](number)
- Har du några tjänster hos [Telia](company)
- hej det kostar [500](number) kr 
- Vad har du för [abonnemang](subscription) idag?
- Okej Det kostar [500](number) kr
- Har du några tjänster hos [Telia](company) idag
- Det kostar [500kr](number)
- är du kund hos oss idag?
- det kostar [500](number) kr 
- är du kund här idag
- hej det kostar [500kr](number)
- har du några tjänster hos oss
- Absolut det blir för [99](number) kr
- Har du några andra tjänster hos oss idag
- Absolut Det blir [90](number) kr
- Har du några andra tjänster här idag
- Absolut Det blir [499 kr](number)
- Har du några tjänster hos [Telia](company) idag
- absolut det blir [50kr](number)
- har du några andra tjänster hos oss idag
- Absolut det blir för [99](number) kr
- Har du några andra tjänster hos [Telia](company) idag
- Absolut det blir för [99](number) kr
- har några andra tjänster hos oss idag
- Super bra, dom kostar [599 kronor](number)
- Har du något [abonnemang](subscription) hos oss?
- Okej, Dom kostar [499 kronor](number)
- Har du ett mobil [abonnemang](subscription)?
- ojdå, mitt misstag, dom ligger på [499 kronor](number)
- Har du ett [mobilabonnemang](subscription) hos oss?
- dom där [hörlurarna](product) är inte så bra och dom kostar [499 kronor](number)
- Har du [abonnemang](subscription) hos oss?
- Jag med, Jag har hört att dom är jätte bra och kostar [499 kronor](number)
- Har du ett [mobilabonnemang](subscription) hos oss?

## intent:gives_discount
- Jag kan ge dig väldigt mycket [rabatt](discount) på dessa [hörlurar](product)
- Vi kan ge mycket [rabatt](discount) på de tjänster du redan använder hos oss!
- Du får [20kr](number) [rabatt](discount)
- Ja du har [46kr](number) [rabatt](discount)
- [10kr](number) [rabatt](discount)
- [10kr](number) [rabatt](discount) menade jag ju såklart
- jag säger att du får det billigare än ordinarie priset Erbjudande gäller bara idag!
- Du får det billigare än ordinarie pris
- du får det billigare än ordinarie pris erbjudandet gäller bara idag
- ok, jag kan ge dig [100 kr](number) [rabatt](discount)
- Ja, det stämmer, men du får dem billigare för du verkar så snäll
- Jag kan ge dig [rabatt](discount)
- du får [100kr](number) i [rabatt](discount) på hörlurarna
- ja du får [100kr](number) [rabatt](discount)
- Ja, du kan få [rabatt](discount)

## intent:gives_no_discount
- I dagsläget har ingen [rabatt](discount) på dessa [hörlurar](product)
- ingen [rabatt](discount)
- nej ingen [rabatt](discount)
- Kan tyvärr inte ge dig någon [rabatt](discount) på dessa [hörlurar](product)
- I dagsläget kan jag inte ge någon [rabatt](discount) på de här [hörlurarna](product)
- I dagsläget har vi ingen [rabatt](discount) på dom där [lurarna](product)
- Tack! Nej, tyvärr, ingen mer [rabatt](discount)
- tyvärr så ingår inte [rabatt](discount)
- tyvärr så ingår inte [rabatt](discount) till [hörlurarna](product) 

## intent:gives_no_discount+q_product_today
- jag kan tyvärr inte ge dig [rabatt](discount)
- hur ser din [bredbandslösning](product) ut?
- tyvärr så kan jag inte ge dig någon [rabatt](discount)
- vad har du för [tv och bredbandslösning](product) idag?

## intent:gives_no_discount+wants_object
- nej ingen [rabatt](discount) kan jag låna [id](object)
- I dagsläget kan jag inte ge någon [rabatt](discount) på de här [hörlurarna](product)
- går det bra att jag får se din [legitimation](object)
- I dagsläget kan jag inte ge någon [rabatt](discount) på de här [hörlurarna](product)
- vad har du för [personnummer](object)
- I dagsläget har vi ingen [rabatt](discount) på dessa [hörlurar](product) men jag ska givetvis se över vilka andra möjligheter som du kan ta del av
- Går det bra att jag får låna din [legitimation](object)?
- Alltså i dagsläget ingen [rabatt](discount) på [hörlurarna](product) men jag skulle kunna se över Vilka andra möjligheter som du kan ta del av
- Går det bra att jag får låna din [legitimation](object)
- I dagsläget har vi ingen [rabatt](discount) på dessa [hörlurar](product) men jag ska givetvis se över vilka andra möjligheter som du kan ta del av
- Går det bra att jag får ditt [Person nummer](object)?
- Alltså i dagsläget ingen [rabatt](discount) på [hörlurarna](product) men jag skulle kunna se över Vilka andra möjligheter som du kan ta del av
- Går det bra att jag får ditt [Personnummer](object)
- I dagsläget i dagsläget har vi ingen [rabatt](discount) på dessa [hörlurar](product) med jag ska se vilka andra möjligheter som du kan ta del av
- går det bra att jag får låna din [information](object)
- I dagsläget har ingen [rabatt](discount) på dessa [hörlurar](product) med ska se vilka andra möjligheter som du kan ta del av
- Går det bra att jag får låna din [legitimation](object)
- I dagsläget kan jag inte dig [rabatt](discount) för [hörlurarna](product) men jag kolla om jag kan ge dig andra erbjudanden
- om jag får se ditt [ID](object) om det är okej
- Jag kan tyvärr inte ge dig någon [rabatt](discount), men jag kan kika på om jag har möjlighet att ge dig andra förmåner
- om det är okej att jag får låna ditt [ID](object) en snabbis?
- Jag kan ej ge dig någon [rabatt](discount)
- men om jag lånar ditt [ID](object) så kan jag nog skrapa fram andra erbjudanden till dig

## intent:q_customer_today
- kund idag?
- Är du kund idag?
- Är du kund hos oss idag?
- Har du några tjänster hos [Telia](company)
- Är du kund hos [Telenor](company)
- Har du några tjänster hos [Telia](company) idag
- Okej, är du kund hos oss idag?
- Är du kund hos [telia](company) idag?
- har du några tjänster hos oss idag?
- har du några tjänster hos oss?
- Ja visst Har du några [mobilabonnemang](subscription) idag hos oss?
- Okej, har du [bredband](product) hos oss?
- Har du [abonnemang](subscription) hos oss?
- Har du mobil [abonnemang](subscription) hos oss på [Telia](company)?
- toppen, Har du [bredband](product) hos oss?
- Men jag måste fråga först, Har du ett [mobilabonnemang](subscription) hos oss idag?
- Måste fråga, Har du ett [abonnemang](subscription) hos oss?
- men jag måste fråga först, har du ett [mobilabonnemang](subscription) hos oss
- Har du [Telia](company) som [abonnemang](subscription)?
- Tack själv, Har du ett [abonnemang](subscription) hos oss föresten?
- Det kan vi fixa, Och har du [bredband](product) hos oss?
- Absolut, Har du [mobilabonnemang](subscription) hos oss?
- Du hade inget mobil [abonnemang](subscription) hos oss?
- du har inte mobil [abonnemang](subscription) hos oss då?
- jag ser att du har [bredband](product) hos oss också
- okej, jag ser att du har [bredband](product) hos oss också
- Har du [bredband](product) hos oss

## intent:wants_object
- kan jag låna ditt [id](object)?
- kan jag få er [legitimation](object)
- få se ert [id](object) lite snabbt
- kan jag få titta på erat [id-kort](object)
- få se på eran [legitimation](object)
- kan jag få ta en titt på ditt [körkort](object)
- Går det bra att jag får titta på din [identifikation](object)
- går det bra att jag får se din [legitimation](object)
- vad har du för [personnummer](object)
- vilket [Personnummer](object) har du
- går det bra om jag får ditt [person nummer](object)
- Absolut det ska vi titta på Då behöver jag bara låna ditt [organisationsnummer](object)
- Absolut det ska vi titta på Då behöver jag bara lite [organisationsnummer](object)
- går det bra om jag får se din [legitimation](object)
- Ja just ja, kan jag få ert [personnummer](object)?
- jaha då ska vi se, kan jag få titta på din [legitimation](object)
- vad är ditt [personnummer](object)?
- Har du ett [ID](object) jag kan låna?
- Om jag får ditt [ID](object) kan jag kanske ge dig bra erbjudanden
- Ge mig ditt [ID](object)
- Tack själv för att du lyssnar, Kan jag låna ditt [ID](object) så kan jag hjälpa dig ännu mer
- jag ska givetvis se över vilka andra möjligheter som du kan ta del av
- Går det bra att jag får låna din [legitimation](object)?
- jag skulle kunna se över Vilka andra möjligheter som du kan ta del av
- Går det bra att jag får låna din [legitimation](object)
- jag ska givetvis se över vilka andra möjligheter som du kan ta del av
- Går det bra att jag får ditt [Person nummer](object)?
- jag skulle kunna se över Vilka andra möjligheter som du kan ta del av
- Går det bra att jag får ditt [Personnummer](object)
- jag ska se vilka andra möjligheter som du kan ta del av
- går det bra att jag får låna din [information](object)
- ska se vilka andra möjligheter som du kan ta del av
- Går det bra att jag får låna din [legitimation](object)
- jag kolla om jag kan ge dig andra erbjudanden
- om jag får se ditt [ID](object) om det är okej
- jag kan kika på om jag har möjlighet att ge dig andra förmåner
- om det är okej att jag får låna ditt [ID](object) en snabbis?
- om jag lånar ditt [ID](object) så kan jag nog skrapa fram andra erbjudanden till dig
- jag jag få titta på din [telefon](product)
- kan jag låna din [telefon](product)
- om du hänger kvar så kan jag se om du har möjlighet att ta del av andra rabatter
- kan jag låna ditt [id](object)?

## intent:ill_help+wants_object
- Absolut, det ska vi titta på, då behöver jag bara låna ditt [organisationsnummer](object)
- det ska jag kunna hjälpa dig med, kan jag få låna ditt [id](object)
- det ska jag fixa år dig, kan jag få ditt [personnummer](object)

## intent:q_product_today
- Vad har du för [tv och bredbandslösning](product) idag?
- vad har du för [bredband](product) idag?
- vad har du för [tv och bredbandslösning](product)
- vad har du för [tv och internetlösning](product) idag?
- okej och vad har du för [tv och bredbandslösning](product) idag?
- Okej, Och hur ser din nuvarande [tv och bredbandlösning](product) ut?
- okej, Hur ser din [bredbandslösning](product) ut?
- Vilken [mobil](product) har du?
- vilken [telefon](product) har du idag?
- Jag förstår, Vilken [mobil](product) har du i dagsläget?
- Vad har du för [mobiltelefon](product)?
- Vad har du i dagsläget?
- vad har du i nuläget
- vad har du för lösning idag?
- vad har du för lösning i nuläget
- vad har du idag
- Vad har du nu?
- hur ser din lösning ut idag?
- Hur mycket [surf](surf) har du i ditt nuvarande [abonnemang](subscription)?
- Hur mycket [surf](surf) har du nu?
- vilket [mobilabonnemang](subscription) har du?
- vad har du för [abonnemang](subscription) idag?
- vad har du för [abonnemang](subscription)?
- vad använder du för mobil [abonnemang](subscription)?
- vilken [surf](surf) mängd har du nu?
- hur mycket [surf](surf) har du idag?

## intent:inform+q_product_today
- jag ser här att du har möjlighet att bli [telia life](service) kund
- vad har du för [tv och bredbandslösning](product) idag?
- Jag förstår och ser att du har möjlighet att bli [Telia Life](service) kund om du byter [bredbandsleverantör](product) till oss
- Vad har du för lösning där idag? 

## intent:promoting_product+q_product_today
- Jag förstår jag ser att du har chansen att bli [Telia Life](service) kund
- vilket innebär att när du samlar dina fasta och mobila tjänster och så får du välja bland olika förmåner exempelvis mer [surf](surf) i mobilen 
- vad du har för [TV av bredbandslösningar](product) då
- Okej jag förstår Jag ser här att du har möjlighet att bli [Telia Life-kund](service)
- vilket innebär att när du samlar dina fasta och mobila tjänster hos oss så får du välja bland olika förmåner som Exempelvis mer [surf](surf) i mobilen
- Vad har du för [tv och bredbandslösning](product) idag?
- jag fattar du kan bli [telia life](service) kund och få mer [surf](surf) i mobilen
- vad har du för [bredband](product) idag?
- Okej jag förstår jag ser att du har möjligt att bli [Telia life](service) kund 
- vilket innebär att när du samlar dina fasta och mobila tjänster så så får du välja bland olika förmåner exempelvis och mer [surf](surf) för mobilen
- vad du har för [TV och bredband](product) lösning idag
- Okej jag förstår jag ser jag ser här att det är möjligt att bli [Telia Life](service) kund 
- vilket innebär att du samlar dina fasta och mobila tjänster hos oss och så får du välja bland olika förmåner exempelvis mer [surf](surf) i mobilen
- vad har för [TV och bredband](product) lösning idag
- okej, Det finns möjlighet för dig att få mer [surf](surf) genom att bli [Telia Life](service) kund
- Vad har du för [bredbandslösning](product) i dagsläget?
- Mer [surf](surf) kan vi fixa om du blir [Telia life](service) kund
- vad har du för [bredband](product) idag?
- Samla dina fasta och mobila tjänster hos oss så du få mer [surf](surf) på köpet
- vad har för [fiber](product) idag?
- Du har möjlighet att bli [Telia Life](service) kund som gör att du kan samla dina fasta och mobila tjänster
- och då får du välja bland massa förmåner som mer surf i mobilen som till exempel
- Vad har du för [tv och bredbandlösning](product)?
- du kan bli [Telia life kund](service) om du samlar dina fasta och mobila tjänster hos oss
- vad har du för [tv och bredbandslösning](product) idag?


## intent:ill_help
- det kan vi ordna
- det kan jag hjälpa dig med
- det ska vi kunna ordna
- självklart, det ska jag givetvis kunna hjälpa dig med
- självklart, det ska jag ordna
- självklart det ska jag givitvis kunna ordna
- okej, det ska jag kunna hjälpa till med
- det ska vi titta på
- jaha det skulle vi kunna hjälpa till med
- absolut, det ska vi titta på
- jättebra, då kan vi fixa det till dig
- ska undersöka det
- det kan jag kolla på
- det kan vi fixa!
- ajdå, ska kolla på det
- jasså, då ska vi kika så att det blir rätt
- då ska jag kolla så allt blir rätt
- okej, vi på telia skulle kunna hjälpa dig och kolla upp vad som är fel

## intent:matter_decided
- Då gör vi så
- Toppen, då fixar vi detta
- då fixar vi det
- då fixar vi detta
- då gör jag det
- då fixar jag det
- då gör jag det till dig
- då fixar jag det till dig
- perfekt, då gör vi så
- då säger vi så
- då kör vi på det
- då kör vi på bara det
- då kör vi bara på det
- då kör vi bara på hörlurarna då
- fixar det till dig jag
- jo det stämmer, då justerar vi det
- Toppen, då har vi en bra lösning för dig
- Toppen, då har vi en bra lösning för dina anställda
- Toppen, då har vi en bra lösning för kunderna som ringer in till företaget
- Toppen, då har vi en bra lösning för dig och dina anställda
- Toppen, då har vi en bra lösning för dig och kunderna som ringer in till företaget
- Toppen, då har vi en bra lösning dina anställda och kunderna som ringer in till företaget
- Toppen, då har vi en bra lösning både dig och dina anställda
- Toppen, då har vi en bra lösning för dig, dina anställda och kunderna som ringer in till företaget

## intent:bye
- Perfekt, hejdå
- Hejdå
- hejdå
- vi ses
- adjö
- vi hörs
- hörs
- Hej då min vän
- Okej, tack, hej då
- tack och hej
- hej då
- HEJ DÅ!
- Okej Tack så mycket Hejdå
- Okej hej då
- Hej då och utgången är där
- Hej då Kund 1
- Hej då leffe
- hej då joel
- Jag sa, Hej då
- okej,Hej då
- Toppen, Tack för det, Hej då
- Okej, vi kommer ingenvart nu, Hej då på dig
- Okej, nu släpper vi det här, Jag säger hejdå
- Tack för det, Då säger jag hej då

## intent:wants_budget
- hur mycket vill du betala?
- okej hur mycket kan du betala?
- hur mycket får en kosta ungefär
- hur mycket är ni villiga att betala?
- det vet jag, men hur mycket vill du betala?
- vilken budget har ni?
- vad är ni beredda att betala?
- strålande, vad är ni beredda att betala?
- hur mycket får ett par [hörlurar](product) kosta
- hur mycket får [bredbandet] kosta per månad?

## intent:q_how_much
- hur mycket?
- okej, hur mycket?
- hur mycket [surf](surf)
- hur mycket [surf](surf) behöver du?
- hur mycket [surf](surf) skulle du vilja ha?
- inga problem, hur mycket [surf](surf)?
- okej inga problem, hur mycket [surf](surf)?
- jag förstår, hur mycket [surf](surf) skulle du vilja ha extra

## intent:q_what
- vilka
- vilka?
- vilken?
- vilka då?
- vilka [kanaler](channels)?
- så vilka [hörlurar](product) vill du ha?
- vad hade du tänkt dig för [hörlurar](product)?
- vilken [telefon](product) hade du tänkt dig?

## intent:scale_evaluation
- skala ett till tio?
- exakt hur nöjda, på en skala ett till tio?
- okej, hur nöjda är ni på en skala ett till tio?
- hur nöjda är ni på en skala ett till tio?
- skala ett till tio, hur nöjd?
- på en skala ett till tio?
- på en skala ett till tio?
- om jag säger att tio är max, vad skulle de få?
- om tio är max betyg?
- hur nöjda är ni med er nuvarande partner, på en skala ett till tio?
- vad skulle er nuvarande partner få för betyg, ifall tio är max
- vad tycker ni om er nuvarande partner, på en skala ett till tio?
- viktigt att värdera nuvarande partners
- hur nöjd är ni på en skala ett till tio?
- självklart är det viktigt att värdera nuvarande partners
- på en skala ett till tio hur nöjd är ni med er nuvarande partner?

## intent:q_do_you_have
- har du [fiber](product)?
- har du några [sportkanaler](channels)
- har du några [sportkanaler](channels)? 
- bra, har du [bredband](product)?
- har du [iphone](product)?
- har du [bredband](product) brorsan?
- nice, har du [sport kanaler](channels)?
- okej, har du [abonnemang](subscription)?
- nice, har du [sport kanaler](channels)
- okej, har du något [bredband](product)?
- jag förstår, och du har [bredband](product)?
- jag måste fråga dig om du har [bredband](product)?
- [internet](product) hemma?
- [sportkanaler](channels) hemma
- jag bara undrar om du har [bredband](product) hemma?

## intent:q_price
- oj förlåt, vilket pris har vi annonserat?
- det vet jag vad vill du betala?
- vilket är det riktiga priset?
- vad ska dom kosta då?
- vad ska de kosta då?
- vad kostar det?
- okej, vad kostar dom?
- vilket pris?
- vad kostar [hörlurarna](product)?
- perfekt, vad kostar dom?

## intent:q_later_meeting
- perfekt när bokar vi nästa möte?
- ska vi ta ett senare möte när ni har mer tid?
- men ska vi boka ett möte i september?
 
## intent:q_what_do_you_like
- vad gillar du
- vad är du intresserad av?
- vad är du intresserad av då?
- okej, vilken sport brukar du titta på?
- vilka sporter är du intreserrad av?
- vilken sport är du intresserad av
- vilka sporter gillar du bäst?
- jaha vilken sport är du intresserad av?
- sedär, vilka sporter är du intresserad av?
- någon speciell sport du är intresserad av?
- vilken sport brukar du titta på?
- vilka sporter gillar du?
- vilka sporter är du intresserad
- vilka sportkanaler är du intresserad av
- okej, vad är det för typ av sport du är gillar då?

## intent:thats_great
- vad roligt
- vad trevligt, klart du ska göra det
- Perfekt
- Perfekt!
- vad kul
- jättekul

## intent:q_should_I_help
- ska jag hjälpa dig med det
- ska jag ta fram en lösning åt dig?
- vill du att jag ska hjälpa dig med det?

## intent:chitchat/out_of_scope_nosense
- hur
- hur?
- hur då
- hur går det till?
- Streamar du mycket på din [surfdata](surf)?
- har vi?
- surf till bredbandet?
- har vi det
- streama film
- jaha, streamar du mycket från mobilen?
- vill du kunna kolla film obegränsat typ?
- vad surfar du på?
- tar det slut ofta?
- jasså, menar du att du har två?
- aha, du menar mars och april, det fixar jag
- vad vill du köpa
- du kanske göra jag måste vänta
- bara sa tack, jag kan erbjudande ett bra erbjudande
- okej, men vill du prata om ditt abonnemang?
- kan jag få köpa din telefon
- jag skulle vilja köpa dessa hörlurar
- idag
- mycket
- telia
- på vilket sätt kommer du använda dina hörlurar?
- vill du byta
- räcker inte det?
- vilket märke är det?

## intent:chitchat/confirming
- jaså det säger du
- jaha ja det menar du inte
- på den punkten var du bestämd
- såklart du har den
- på så sätt
- är du helt säker?
- heter dom så?

## intent:chitchat/compliment
- snyggt foto
- bra jobbat
- bra svar
- bra tänkt

## intent:chitchat/weather
- fint väder idag
- varmt ute idag
- soligt ute
- fint väder
- väder
- är det fint väder
- I alla fall fint väder
- härligt väder
- underbart väder idag
- Fint väder idag förresten?
- Är det soligt idag

## intent:chitchat/weather_bad
- så ni gör bara nya affärer när det är dåligt väder?
- regnar mycket idag
- riktigt kallt ute idag
- lite kallt är det allt
- Nja lite kallt är det allt
- Konstigt att du tycker det är fint väder när det regnar
- fast det regnar ju
- det regnar
- det regnar idag
- Nja, det regnar ju
- det regnar ju
- regnar mycket idag
- Gillar du regn?

## intent:chitchat/thanks
- Tack
- Tackar
- tack
- tackar
- Tack så mycket
- tack så mycket
- man tackar
- Man tackar
- Tack snälla
- tack snälla
- Tack för att du köpte dom här hörlurarna
- Tack själv
- tack för de 3 poäng som jag fick

## intent:chitchat/handle_insult
- dum bot
- idiot
- fuck off
- är du dum i huvudet?
- dra år helvete
- jävla idiot
- mongo

## intent:chitchat/handle_slight_insult
- Nu får du lugna dig lite
- Lugna ner dig är du snäll
- Okej, ta det lugnt
- Vad aggressiv du är
- är du arg?
- Nu får du gå faktiskt det lite tröttsamt
- Nu kommer nästa kund
- Nu tycker jag du är lite otrevlig
- gå från min butik
- Ja dom är gratis
- 2 meters avstånd, Tänk på smittan
- Jag måste ta nästa kund nu, Du får akta dig lite
- Jaja, Du behöver inte vara otrevlig
- Nu är du lite tjatig
- Du får gå härifrån
- Vill du gå härifrån?
- Jag ja, nu kommer nästa kund, Hej då
- okej då har vi ett problem
- Ja men du är otrevligare än vad du annonserat
- Du är intresserad av att svara på frågan eller?
- Som jag säger till exempel
- Nu är det du som är otrevlig
- och, vem bryr sig
- jag tycker vi går vidare härifrån på varsitt håll!
- det här fungerar inte
- det blir inget av det här känner jag
- Nu får du gå
- Det var ju lite off
- tror inte du har råd att köpa dom [hörlurarna](product) utifrån dina kläder
- att du säger samma sak hela tiden

## intent:chitchat/all_good
- allt är bra med mig
- allt bra här
- det är bra med mig
- jo det är fint med mig
- det är fint med mig
- jo men det är bra med mig
- det är bra
- allt är bra

## intent:chitchat/greet
- hej
- tjena
- hejsan
- tjenare
- hallå
- hello
- Tjena
- Hejsan
- Hej
- hej du
- goddag
- God kväll
- god förmiddag
- god eftermiddag
- god dag
- God Kväll

## intent:chitchat/what_is_your_name
- Vad heter du?
- Vem är du
- Är du en bot?
- och du är?
- vad heter du?
- Vem är du då?
- Vad heter du
- vem är du?

## intent:chitchat/what_can_you_do
- vad kan du göra?
- vad kan du?
- Vad kan du göra för någonting
- vad kan du säga för något

## intent:chitchat/whats_up
- hur är läget?
- hur är läget
- läget
- läget då
- hej hur är läget
- tjena, läget
- tjena hur är läget
- hej, läget?
- hur mår du?

## intent:chitchat/out_of_scope_other
- hur mycket väger du?
- jag vill äta pizza
- hur många timmar går det på ett år?
- har du körkort?
- varför är jorden rund
- vad är mitt personnummer
- så du är singel
- sätt på tv:n
- hej google
- okej men vad har dom för plan?
- nej tyvärr de compostela idag
- jag vill ha en fallback

## intent:chitchat/weird_deal
- du får [hörlurarna](product) av mig
- du får dom gratis
- okej, men du kan få ett gratis mobilskal
- dom kostar endast [499 kronor](number) om man är [telia life](service) kund hos oss

## intent:chitchat/negotiating
- om jag sänker priset då?
- ojdå men om jag sänker priset rejält?
- om du köper mer [surf](surf) så kostar [hörlurarna](product) [500kr](number)
- du får [hörlurar](product) för [500kr](number) om du köper [abonnemang](subscription)
- ja men om du uppgraderar ditt [abonnemang](subscription) så kan du få dom för [500kr](number)
- jag bjuder på kaffe om du köper [hörlurarna](product)
- jag bjuder på kaffe om du köper två par [hörlurar](product)
- skulle du behöva något annat? kanske en kaffe?

## intent:chitchat/sorry
- jag är ledsen
- förlåt
- jag ber om ursäkt
- du har så rätt, förlåt
- mitt fel, förlåt så mycket
- du har rätt, jag är ledsen för mitt misstag
- mitt fel jag fixar det
- det stämmer jag ber om ursäkt
- Okej, förlåt

## intent:chitchat/tell_a_joke
- Berätta ett skämt
- lite trist stämning nu, kan du berätta ett skämt
- få höra ett skämt
- har du någon rolig historia på lager
- kan du dra en rolig historia
- kan du berätta ett roligt skämt

## intent:chitchat/complementing_choice
- prisvärda som attans
- de är grymma
- okej de är grymma
- nice bra val
- okej bra val de är grymma
- prisvärda är dom
- prisvärt

## intent:chitchat/like_football
- Härligt Gillar du fotboll?
- gillar du fotboll brorsan
- gillar du fotboll
- tycker du om fotboll
- du ser ut att gilla fotboll
- tycker du om att kolla på fotboll
- du ser ut som en person som gillar fotboll
- du ser ut som en fotbollstjej
- du ser ut som en fotbollskille
- ser du mycket på fotboll

## intent:chitchat/like_movies
- Okej, Gillar du se film på mobilen?
- Gillar du film?
- ser du mycket på film
- kollar du mycket på film
- kollar du mycket på film i mobilen
- ser du på mycket film
- hur mycket film kollar du på 
- Gillar du Netflix?
- Surfar du netflix?
- Jag tänkte om du streamar mycket film eller serier?
- Brukar du kolla film på mobilen?
- brukar du streama film och ladda ner saker?
- Streamar du mycket film?

## intent:chitchat/like_series
- Vilka serier gillar du?
- kollar du mycket på serier
- vilken är din favorit serie
- hur mycket serier kollar du på
- vilka serier på netflix kollar du på?
- kollar du mycket på serier i mobilen
- ser du på serier i mobilen

## intent:chitchat/like_sports
- Gillar du sport?
- Är sport något som intresserar dig
- Är du sportintresserad?
- du ser ut som en sportkille
- du ser ut som en sporttjej
- gillar du att kolla på sport
- kollar du mycket på sport
- tycker du om sport
- tycker du om att kolla på sport
- du verkar vara en person som gillar mycket sport
- Men du verkar vara en person som gillar mycket sport
- ser du mycket på sport
- Jaha härligt, Är du en sport kille?
- Jag förstår, Är du sportintresserad?

## intent:chitchat/like_tv
- Förlåt, om du tog illa upp, Tittar du mycket tv?
- tittar du mycket på tv
- gillar du att titta på tv
- hur mycket tv tittar du på
- ser du mycket på tv
- gillar du att se på tv
- Ser du mycket på TV
- Vad brukar du kolla på tv?
- Brukar du kolla tv?
- Okej, Vad gillar du titta på tv?
- vad gillar du att titta på tv?

## intent:chitchat/prepares_sale_pitch
- Jag har en grym lösning till er
- jag har en mycket bättre lösning till er
- jag har den perfekta lösningen
- jag har ett erbjudande du inte kan säga nej till
- Jag kan fixa en bra deal till dig
- jag har ett erbjudande till dig
- Jag kan hjälpa dig få förmåner
- jag förstår, jag tänkte att jag skulle kunna erbjuda dig lite förmåner
- Perfekt vi har detta erbjudandet
- vi har detta erbjudande
- Kan jag berätta om Telias erbjudanden
- Kan jag berätta vad det handlar om?
- vill du ha en bra deal?
- men du, jag skulle kunna fixa förmåner till dig, 
- jag har en bra deal du inte får missa
- vi har en bra tjänst som kan lösa dina problem

## intent:chitchat/negations
- Vilken sport är du inte intresserad av
- Vilka sporter gillar du inte?
- Vilka sporter är du inte intresserad av?
- Vilka sporter gillar du sämst?
- sedär, vilka sporter är du inte intresserad av då?
- någon speciell sport du inte är intresserad av?
- Vilka [sportkanaler](channels) är du inte intresserad av
- Vad vill du inte?
- Vad vill du inte köpa
- Vad är det för problem du inte har upplevt?
- Vad använder du inte för mobil abonnemang?
- vilket [mobilabonnemang](subscription) har du inte?
- Vilket märke är det inte?
- nej tyvärr, vad har du inte för abonnemang?
- jaha vilken sport är du inte intresserad av?
- Okej, Vad är det för typ av sport du inte gillar då?
- Vad är du inte intresserad av?
- Vad är du inte intresserad av då?

## intent:chitchat/wants_payment_method
- vill du betala med kort
- vill du betala kontant
- betalar du med kort eller kontant
- kort eller kontant?
- Jag antar du vill betala med kort
- vill du betala med kort eller kontant
- Nåväl, antar att du betalar med kort

## intent:chitchat/nagging
- jo, kom igen
- snälla
- kom igen då
- Kom igen brorsan
- Kom igen, Varför spelar du svår?

## intent:chitchat/tell_me
- berätta mer
- kan du visa mig
- låt höra
- kan du utveckla

## intent:chitchat/clueless
- Jag förstår inte
- jag fattar inte vad du menar
- Vad menar du?
- kan du förtydliga
- Kan du upprepa vad du sa
- kan du säga det igen
- vad sa du
- va sade du
- kan du vara snäll att upprepa
- jag hörde inte riktigt
- ursäkta, vad sade du?
- det vet jag inte
- vet inte
- ja vad menar du?
- vad menar du?
- Jaha vad har det med saken att göra?
- vad har det med saken att göra

## intent:chitchat/q_why
- varför?
- varför då?
- varför
- varför då
- Okej Varför?
- Varför inte
- Varför vill du köpa dessa [hörlurar](product)?
- varför borde jag det?
- Varför prata du om det?



