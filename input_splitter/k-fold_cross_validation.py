from train_model import KerasModel
from pathlib import Path
from pickle_featurizer import SparseFeaturizer, GetData, TextPreproccessing
import sklearn
import numpy as np
from evaluate_model import ExactMatch
import pickle
from utils.constants import (
    MAX_WORDS,
    PATH_TO_DATA,
    N_SPLITS,
    )

if __name__ == "__main__":
    # Runs a k-fold crossvalidation and saves the results in a pickle.
    path_to_NLU = Path(PATH_TO_DATA)
    utterings, labels = GetData(path_to_NLU)
    results_pickle = "evaluation_results.pkl"

    utterings_array = np.asarray(utterings)

    kf = sklearn.model_selection.KFold(n_splits=N_SPLITS, shuffle=False, random_state=None)
    loss_list = []
    recall_list = []
    precision_list = []
    hammingloss_list = []
    binary_acc_list = []
    exact_match_score_list = []
    loop_id = 0
    for trainindex, testindex in kf.split(utterings):
        # Extract train and test data from indices
        x_train = utterings_array[trainindex].tolist()
        y_train = labels[trainindex]

        x_test = utterings_array[testindex].tolist()
        y_test = labels[testindex]
        
        # Create the featurizer from training data
        feat = SparseFeaturizer(x_train, n=5)
        # Make tensors out of input and labels
        x_train_tensor, y_train_tensor = TextPreproccessing(feat, x_train, y_train)
        x_test_tensor, y_test_tensor = TextPreproccessing(feat, x_test, y_test)

        # Train the model
        model = KerasModel(feat, x_train_tensor, y_train_tensor)

        # Evaluate the model
        loss, precision, recall, binary_acc, hammingloss = model.evaluate(
            x = x_test_tensor, 
            y= y_test_tensor,
            )
        exact_match = ExactMatch(model, x_test,x_test_tensor, y_test, verbose = False)

        loss_list.append(loss)
        recall_list.append(recall)
        precision_list.append(precision)
        hammingloss_list.append(hammingloss)
        binary_acc_list.append(binary_acc)
        exact_match_score_list.append(exact_match)
        loop_id += 1
        print(str(loop_id)+"/"+str(N_SPLITS)+" done")
    with open(results_pickle, 'wb') as f:
        pickle.dump([loss_list, recall_list, precision_list, hammingloss_list, binary_acc_list, exact_match_score_list], f)
