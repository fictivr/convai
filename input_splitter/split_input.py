from pathlib import Path
import nltk
import re
from nltk.tokenize import word_tokenize
from typing import List, Optional, Text, Tuple, Callable, Union, Any
import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
from tensorflow.keras.utils import model_to_dot, Sequence
from tensorflow.python.eager import context
from utils.transformer import TransformerEncoder
from utils.layers import SparseDropout, DenseWithSparseWeights, Ffnn, DenseForSparse, Embed
from pickle_featurizer import SparseFeaturizer
import sys
import pickle
import random

from utils.constants import (
    UNITS,
    TRANSFORMER_LAYERS,
    MULTI_ATTENTION_HEADS,
    REGULARIZATION_LAMBDA,
    TRANSFORMER_SPARSITY,
    TRANSFORMER_DROPOUT_RATE,
    EMBEDDING_DIMENSIONS,
    MAX_WORDS,
    BATCH_SIZE,
    INPUT_SPARSITY,
    EPOCHS,
    RUN_EAGERLY,
    THRESHOLD,
    )

    
class SplitInput():
    def __init__(self, model, feat):
        self._model = model
        self._feat = feat

    def __call__(self, row):
        if row != "":
            row = re.sub("\]\((.*?)\)", "", row)
            row = re.sub("\[|\?|\.|\,|\!|\;|\:|\]|\-|\/", "", row)
            inp = row
            row = row.lower()
            row = re.sub("\d+", "D", row)
            tokenized_sent = word_tokenize(row)
            if len(tokenized_sent) > MAX_WORDS:
                tokenized_sent = tokenized_sent[:MAX_WORDS]
            input_sent = [tokenized_sent]
            tensor = self.SentencesToTensor(input_sent)
            prediction = self._model.predict(tensor)
            result = (prediction > THRESHOLD) * np.ones(np.shape(prediction))
            ind = 0
            token_list = []
            len_sent = len(tokenized_sent)
            for _ in range(int(len(result[0])/2)):
                binary = result[0][ind:ind+2]
                ind += 2
                # I am changing the binary output to the S,C and E tokens for the purpose of presentation in my report
                # So it does nothing and could be removed (In that case TokensToSegments also needs to adapt that change) 
                if (binary == [0,1]).all():
                    token_list.append("S")
                elif (binary == [1, 1]).all():
                    token_list.append("C")
                elif (binary == [1, 0]).all():
                    token_list.append("E")
                else:
                    token_list.append(str(0))
            token_list = token_list[:len_sent]
            output_sentences = self.TokenToSegments(word_tokenize(inp), token_list)
        return output_sentences
    
    def TokenToSegments(self, sent, tokens):
        output_sents = []
        #output_sent = []
        for idx, token in enumerate(tokens):
            #if idx+1 == len(tokens):
            #    print("End of sentence")
            if token == "S":
                output_sent = []
                output_sent.append(sent[idx])
            elif token == "C":
                output_sent.append(sent[idx])
            elif token == "E":
                # The "E" (end) token marks a complete sentence
                # I can think of problems that can appear here:
                # If a incomplete sentence is detected, ex ["S", "C", "C", "C", "C"] the splitter wont return it even if it might be better
                # Changing the code to return the sentence it if its the last token will lead to a different problem.
                # When an input with a length greater than MAX_LENGTH is processed you wouldnt want to return it
                # Example ["S", "C",...,"C", "E" ,"S", "C", "C", "C"] you would want it to split it to ["S", "C",...,"C", "E"] and the ["S", "C", "C", "C"] part would be processed in the next iteration
                output_sent.append(sent[idx])
                output_sents.append(" ".join(output_sent))
        return output_sents

    def SentencesToTensor(self, x):
        # Model wants tensors as input, this method is making that conversion.
        x_tens = tf.sparse.expand_dims(self._feat(x[0]), axis=0)
        for _, sent in enumerate(x[1:]):
            tensor_x = tf.sparse.expand_dims(self._feat(sent), axis=0)
            x_tens = tf.sparse.concat(axis=0, sp_inputs=[x_tens, tensor_x], expand_nonconcat_dims=False, name=None)
        return x_tens

if __name__ == "__main__":
    path_to_model = "models/2020-12-01-11-43.h5"
    pickles = "data/variables.pkl"
    with open(pickles, 'rb') as f:  # Python 3: open(..., 'rb')
        feat, x_tensor, labels_tensor = pickle.load(f)
    model = tf.keras.models.load_model(path_to_model, custom_objects={
        'SparseDropout': SparseDropout,
        'DenseForSparse': DenseForSparse,
        'TransformerEncoder': TransformerEncoder,
        'Embed': Embed,
        'HammingLoss': tfa.metrics.HammingLoss,
        }
        ,compile=True)
    splitter = SplitInput(model, feat)
    
    print(splitter("Det blir 400 kr är du kund hos telia idag?"))
