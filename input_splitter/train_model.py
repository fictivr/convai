from datetime import datetime
import sys, pickle
from pickle_featurizer import SparseFeaturizer
from utils.layers import SparseDropout, DenseForSparse, Embed
from utils.transformer import TransformerEncoder
import tensorflow as tf
import tensorflow_addons as tfa

from utils.constants import (
    UNITS,
    TRANSFORMER_LAYERS,
    MULTI_ATTENTION_HEADS,
    REGULARIZATION_LAMBDA,
    TRANSFORMER_SPARSITY,
    TRANSFORMER_DROPOUT_RATE,
    EMBEDDING_DIMENSIONS,
    MAX_WORDS,
    BATCH_SIZE,
    INPUT_SPARSITY,
    EPOCHS,
    RUN_EAGERLY,
    THRESHOLD,
    )


def KerasModel(sparse_featurizer, x_tensor, y_tensor):

    inp = tf.keras.Input(shape=(MAX_WORDS+1, sparse_featurizer.word_input_dim,), sparse=True, batch_size=BATCH_SIZE)
    sparse_from_getgo = SparseDropout(rate = INPUT_SPARSITY)(inp)
    l1 = DenseForSparse(units=UNITS, activation='relu')(sparse_from_getgo)
    transformer_layer = TransformerEncoder(
        num_layers = TRANSFORMER_LAYERS,
        units = UNITS,
        num_heads = MULTI_ATTENTION_HEADS,
        filter_units = 4*UNITS,
        reg_lambda = REGULARIZATION_LAMBDA,
        dropout_rate = TRANSFORMER_DROPOUT_RATE,
        attention_dropout_rate = 0.0,
        sparsity = TRANSFORMER_SPARSITY,
        unidirectional = False,
        use_key_relative_position = False,
        use_value_relative_position = False,
        max_relative_position = None,
        heads_share_relative_embedding = False,
        name = "TransformerLayers",
        )(l1) 

    embedding_layer = Embed(
        embed_dim = EMBEDDING_DIMENSIONS,
        reg_lambda = REGULARIZATION_LAMBDA,
        layer_name_suffix = "EmbeddingLayer",
        similarity_type = None,
        )(transformer_layer)

    flatten = tf.keras.layers.Flatten(name="Concatenate")(embedding_layer)
    dense = tf.keras.layers.Dense(MAX_WORDS*2, activation='sigmoid')(flatten) # Multi-label classification, dont use softmax

    model = tf.keras.Model(inputs=inp,outputs=dense)
    precision = tf.keras.metrics.Precision(thresholds=THRESHOLD, name="Precision")
    recall = tf.keras.metrics.Recall(thresholds=THRESHOLD, name="Recall")
    binary_accuracy = tf.keras.metrics.BinaryAccuracy(threshold=THRESHOLD, name="Binary Accuracy")
    hamming_loss = tfa.metrics.hamming.HammingLoss(threshold=THRESHOLD, mode="multilabel", name="Hamming Loss")

    ## run_eagerly = True | To debug
    loss_function = tf.keras.losses.BinaryCrossentropy()
    model.compile(
        optimizer = tf.keras.optimizers.RMSprop(learning_rate=1e-3),
        loss = loss_function, run_eagerly=RUN_EAGERLY,
        metrics = [precision, recall, binary_accuracy, hamming_loss],
        )
    
    # Train model on dataset
    model.fit(
        x=x_tensor,
        y=y_tensor,
        batch_size=BATCH_SIZE,
        epochs=EPOCHS,
        verbose=1,
        callbacks=None,
        validation_data=None,
        shuffle=True, 
        class_weight=None,
        sample_weight=None,
        initial_epoch=0,
        steps_per_epoch=None,
        validation_steps=None,
        validation_freq=1,
        max_queue_size=10, 
        workers=1,
        use_multiprocessing=False
        )
    
    return model

if __name__ == "__main__":
    model_name = str(datetime.now()).replace(" ", "-")[:-10]
    pickle_path = "data/variables.pkl"
    path_to_model = "models/" + model_name.replace(":", "-") +".h5"
    try:
        with open(pickle_path, 'rb') as f:  # Python 3: open(..., 'rb')
            feat, utterings, labels = pickle.load(f)
    except: 
        print("Error: Could not load get " + pickle_path)
        sys.exit(1)
    
    model = KerasModel(feat, utterings, labels)
    model.save(path_to_model)
    print("Model saved at: "+path_to_model)
