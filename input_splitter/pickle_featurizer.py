from pathlib import Path
from nltk.tokenize import word_tokenize
import sklearn
import tensorflow as tf
import numpy as np
import re, sys, pickle, random
from utils.constants import (
    MAX_WORDS,
    TEST_SIZE,
    PATH_TO_DATA,
    MIN_WORDS,
    SYNTHESIZED_SAMPLES,
    N_SPLITS,
    )

class SparseFeaturizer():
    # The class that maps words to sparse vectors
    # The mapping is created from the training data and is "unique" for the training data that was used 
    # The object it created with a corpus, 
    # When the class object is called it is returning a tensor of the vector representation
    def __init__(self, corpus, n=5):
        self.max_words = MAX_WORDS
        self.corpus = corpus
        self.setofwords = self.SetOfWords()
        self.list_of_grams = self.MakeNGrams(n)
        self.word_input_dim = self.WordInputDim()

    def SetOfWords(self):
        # Used the bag-of-words one-hot representation
        # It is alphabeticaly sorted here which isnt theoreticaly needed
        # But I dont think it would speed up training noticeably and I am not sure which additional changes that would require
        list_of_words = [word for utterance in self.corpus for word in utterance]
        data = set(list_of_words)
        data = list(data)
        data = sorted(data)
        return data

    # The following three functions is to make the one-hot-encoding representation for all existing n-grams of all words in corpus
    def SetOfNGrams(self, n):
        list_of_ngrams = []
        for word in self.setofwords:
            ngrams = self.NGrams(word, n)
            for ngram in ngrams:
                list_of_ngrams.append(ngram)
        data = set(list_of_ngrams)    
        data = list(data)
        data = sorted(data)
        return data

    def MakeNGrams(self, n):
        list_of_ngrams = []
        for i in range(n):
            list_of_ngrams.append(self.SetOfNGrams(i+1))
        return list_of_ngrams

    def NGrams(self, word, n):
        chars = [char for char in word]
        ngrams = zip(*[chars[i:] for i in range(n)])
        ngrams = ["".join(ngram) for ngram in ngrams]
        return ngrams

    def SparseTensorWord(self, word, n=5):
        # Outputs the sparse vector representaition of the input word
        # I am unsure why I have a default n as 5 here, not sure if changing the number of ngrams used in constants would work...
        index = 0
        value = 0
        if word in self.setofwords:
            index = self.setofwords.index(word)
            value = 1
        tens = tf.sparse.SparseTensor(indices=[[index]], values=[value], dense_shape=[len(self.setofwords)])
        for idx, grammap in enumerate(self.list_of_grams):
            indices = []
            values = []
            for gram in self.NGrams(word, idx+1):
                if gram in grammap:
                    index = grammap.index(gram)
                    if [index] not in indices:
                        indices.append([index])
                        values.append(1)
                    ## Mute else statement for only ones
                    # Unsure what is correct here but this seem to work but it may be better to use only ones.
                    else: 
                        values[indices.index([index])] += 1
                else:
                    values.append(0)
                    indices.append([0])
            if len(indices) > 0:
                gram_tensor = tf.sparse.SparseTensor(indices=indices, values=values, dense_shape=[len(grammap)])
            else:
                gram_tensor = tf.sparse.SparseTensor(indices=[[0]], values=[0], dense_shape=[len(grammap)])
            sp_inputs = [tens, gram_tensor]
            tens = tf.sparse.concat(axis=0, sp_inputs=sp_inputs, expand_nonconcat_dims=False, name=None)
        return tens

    def __call__(self, sentence):
        # Rows x Columns -> wordvector x maxwords | USE THIS
        # The cls_tok is the sentence token, i.e all word representions in the sentence added together
        cls_tok = self.SparseTensorWord(sentence[0], 5)
        sentence_tensor = cls_tok
        for word in sentence[1:]:
            word_tensor = self.SparseTensorWord(word)
            sentence_tensor = tf.sparse.concat(axis=0, sp_inputs=[sentence_tensor, word_tensor], expand_nonconcat_dims=False, name=None)
            cls_tok = tf.sparse.add(cls_tok, word_tensor)
        for _ in range(len(sentence),self.max_words):
            word_tensor = self.SparseTensorWord("")
            sentence_tensor = tf.sparse.concat(axis=0, sp_inputs=[sentence_tensor, word_tensor], expand_nonconcat_dims=False, name=None)
        sentence_tensor = tf.sparse.concat(axis=0, sp_inputs=[sentence_tensor, cls_tok], expand_nonconcat_dims=False, name=None)
        is_nonzero = tf.not_equal(sentence_tensor.values, 0)
        sentence_tensor = tf.sparse.retain(sentence_tensor, is_nonzero)
        sentence_tensor = tf.sparse.reshape(sp_input=sentence_tensor, shape=[self.max_words+1 ,word_tensor.get_shape().as_list()[0]])
        return sentence_tensor

    def WordInputDim(self):
        # Just to set the attribute of the length of the vector representation
        input_dim = len(self.setofwords)
        input_dim += sum([len(gram) for gram in self.list_of_grams])
        return input_dim

def GetData(path_to_corpus):
    # Reads data, creates labels and synthesize data with labels.
    # This function will not work on RASA 2.x data format
    # The output is a utterings as a lists of words in a list of lists and the labels corresponding to that in a similar fashion
    with open(path_to_corpus) as f:
        max_uttering_length = 0 # Not needed, just for debugging
        single_utterences = []
        boolean = False
        for row in f:
            # This logic is to only look at intents and not synonyms or non swedish
            if row[:43] == "## intent:chitchat/out_of_scope_non_swedish":
                boolean = False
            elif row[:6] == "## int":
                boolean = True
            elif row[:2] == "##":
                boolean = False
            elif boolean == True:
                ## Entity classification removal i.e "[telefon](product)" becomes "telefon"
                row = row.rstrip()
                if row != "":
                    row = row.lower() # only lowercase letters
                    row = re.sub("\]\((.*?)\)", "", row) # Entity classification removal
                    row = re.sub("\[|\?|\.|\,|\!|\;|\:|\]|\-|\/", "", row) # removes special characters
                    row = re.sub("\d+", "D", row) # change all digits to capital D
                    tokenized_sent = word_tokenize(row) # whitespace tokenizer
                    if len(tokenized_sent) >= MAX_WORDS: # limit sentence length to the specified maximum in utils/constants
                        tokenized_sent = tokenized_sent[:int(MAX_WORDS)]
                    if tokenized_sent not in single_utterences: # Make sure all utterences are unique
                        single_utterences.append(tokenized_sent)             
                    if len(tokenized_sent) > max_uttering_length: # Not needed, will try to delete it later
                        max_uttering_length = len(tokenized_sent)
    ## Synthesizing multi intent utterings
    utterences = single_utterences.copy()
    multi_utterences = []
    multi_utterences_labelarray = np.zeros((SYNTHESIZED_SAMPLES, MAX_WORDS*2))
    min_length = MIN_WORDS # Minimum length of the splitted intents as you wouldnt, for example, want single word intents splitted  
    i = 0
    while i < SYNTHESIZED_SAMPLES:
        first = random.choice(utterences)
        if len(first) >= min_length and len(first) < (MAX_WORDS-min_length):
            second = random.choice(utterences)
            if len(second) >= min_length and len(second) < (MAX_WORDS-min_length):
                multi_utterence = first+second
                if multi_utterence not in multi_utterences and len(multi_utterence) < MAX_WORDS:
                    multi_utterences.append(multi_utterence)
                    multi_utterences_labelarray[i] = MultiUtteringLabel(first, second)
                    i += 1
            else:
                utterences.remove(second)
        else:
            utterences.remove(first)  
    single_utterences_labels = SingleUtteringLabels(single_utterences)
    label_array = np.concatenate((single_utterences_labels, multi_utterences_labelarray),axis=0)
    utterings = single_utterences + multi_utterences
    x, y = sklearn.utils.shuffle(utterings, label_array) # Finishing with shuffling the data to remove the problems that comes with sorted data
    return x, y

def MultiUtteringLabel(first, second):
    # Creates labels for double intent sentences
    # first = first intent
    # second = second intent
    # In the future in might be relevant to make this general, i.e it creates label for utterings with n intents.
    label = np.zeros(MAX_WORDS*2)
    label[1:len(first)*2-1] = 1
    label[len(first)*2+1:(len(first)*2 + len(second)*2-1)] = 1
    return label

def SingleUtteringLabels(data):
    # Creates labels for single intent sentences
    out_array = np.zeros((len(data), MAX_WORDS*2))
    for k,sent in enumerate(data):
        if len(sent) > 1:
            out_array[k][1:len(sent)*2-1] = 1
        else:
            out_array[k][1] = 1
    return out_array

def TextPreproccessing(sparse_featurizer, x, y, progress_bar = True):
    # Creates tensors out of the utterings and labels.
    # As this takes some time I added a progress bar
    x_tens = tf.sparse.expand_dims(sparse_featurizer(x[0]), axis=0)
    label_tensor = tf.expand_dims(tf.constant(y[0]), axis=0)
    if progress_bar:
        toolbar_width = 40
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        w = 0
    for i, sent in enumerate(x[1:]):
        if progress_bar:
            if int(round(toolbar_width*(i/(len(x)-1)))) != w:
                w = int(round(toolbar_width*(i/(len(x)-1))))
                sys.stdout.write("-")
                sys.stdout.flush()
        tensor_x = tf.sparse.expand_dims(sparse_featurizer(sent), axis=0)
        x_tens = tf.sparse.concat(axis=0, sp_inputs=[x_tens, tensor_x], expand_nonconcat_dims=False, name=None)
        tensor_l = tf.expand_dims(tf.constant(y[i+1]), axis=0)
        label_tensor = tf.concat([label_tensor, tensor_l], axis = 0)
    if progress_bar:
        sys.stdout.write("]\n")
    label_tensor = y
    return x_tens, label_tensor  

if __name__ == "__main__":
    path_to_NLU = Path(PATH_TO_DATA)
    pickle_path = "data/variables.pkl"
    utterings, labels = GetData(path_to_NLU)
    
    feat = SparseFeaturizer(utterings, n=5)
    utterings_tensor, labels_tensor = TextPreproccessing(feat, utterings, labels)
    with open(pickle_path, 'wb') as f:
        pickle.dump([feat, utterings_tensor, labels_tensor], f)
    print("Saved as variables.pkl")
