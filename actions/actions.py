# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

# -*- coding: utf-8 -*-
import logging
import json
import requests
import pandas as pd
import re
from datetime import datetime
from typing import Any, Dict, List, Text, Union, Optional

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.events import (
    SlotSet,
    UserUtteranceReverted,
    ConversationPaused,
    EventType,
    FollowupAction,
    Restarted,
    AllSlotsReset,
    ActionReverted
)

from actions import config

logger = logging.getLogger(__name__)

INTENT_DESCRIPTION_MAPPING_PATH = "actions/intent_description_mapping.csv"
SCENARIO_DESCRIPTION_PATH = "actions/scenario_description.csv"
PRICE_PHONE = 499
PRICE_TVINTERNET = 499
PRICE_HEADPHONES = 499
FALLBACK_DICTIONARY = {
    "utter_intro": "Hej, jag skulle vilja köpa dessa hörlurar",
    "fallback0_intro": "Som jag sa, jag vill köpa hörlurarna, vad kostar dom?",
    "fallback0_you_wanna_ask_something": "Du borde fråga om jag är kund idag",
    "fallback1_you_wanna_ask_something": "Fråga om jag är kund idag så kan vi gå vidare",
    "fallback0_shouldnt_you_ask_for_id": "Nu har du glömt något",
    "fallback1_shouldnt_you_ask_for_id": "Eftersom jag är kund idag så bör du fråga efter legitimation för att få min information",
    "fallback0_question_discount": "Jag undrar om du kan ge mig någon rabatt",
    "fallback0_ofcourse": "Samtidigt som du kollar upp min information kan det vara läge att fråga om jag är nöjd med min nuvarande produkt",
    "fallback0_sad_but_sure": "Det kommer ta några sekunder att kolla upp kundinformationen så fråga om jag är nöjd med min nuvarande produkt under den tiden",
    "fallback0_happy_more_surf": "Ta läget att väcka intresse kring lösningar du kan presentera senare",
    "fallback0_interest": "Då jag redan har mina mobila tjänster hos er idag så borde du vara intresserad av mina fasta tjänster",
    "fallback0_more_surf": "Då jag redan har mina mobila tjänster hos er idag så borde du vara intresserad av mina fasta tjänster",
    "fallback0_tvinternet_how": "Nu vet du att jag har tv och bredband via fiber ifrån en annan leverantör så du bör ta reda på vad jag tycker om den lösningen",
    "fallback0_tvinternet_inform": "Fråga om det saknas något gällande tv och bredband",
    "fallback0_wants_sports": "Det finns många sportkanaler, det kan vara bra att veta lite mer specifict",
    "fallback0_wants_football": "Fråga om jag är missnöjd med något i lösningen jag har idag",
    "fallback0_tvinternet_unhappy": "En liten felsökning kan vara bra att göra",
    "fallback0_live_alone": "Summera det vi har pratat om",
    "fallback0_internet_usage": "Summera det vi har pratat om",
    "fallback0_confirm": "Försäkra att det inte är något mer du behöver ta hänsyn till"
}


class ActionPrintEventHistory(Action):
    def name(self):
        return "action_print_history"
    def run(self, dispatcher, tracker, domain):
        print("---------------------------")
        for item in tracker.current_state()["events"]:
            if item["event"] == "action":
                print(item["event"],": ", item["name"])
            else:
                try:
                    print(item["event"],": ", item["text"])
                except:
                    print(item["event"],": ", item["timestamp"])
        print("------------------------")
        return []

class EndOfConv(Action):
    def name(self):
        return "action_end_of_conversation"

    def run(self, dispatcher, tracker, domain):
        value = tracker.current_slot_values().get("feedback_value")
        if value == None:
            value = 0
        output_text = "Du fick " + str(value) + " poäng"
        dispatcher.utter_message(text=output_text)
        return [Restarted(), AllSlotsReset()]

class AddPoint(Action):
    def name(self):
        return "action_add_point"
    
    def run(self, dispatcher, tracker, domain):
        value = tracker.current_slot_values().get("feedback_value")
        try:
            if value == None:
                value = 1
            else:
                value = int(value) + 1
        except:
            dispatcher.utter_message(text="AddPointsError, points is not an int")
        return [SlotSet("feedback_value", int(value))]

class RetractPoint(Action):
    def name(self):
        return "action_retract_point"
    
    def run(self, dispatcher, tracker, domain):
        value = tracker.current_slot_values().get("feedback_value")
        try:
            if value == None:
                value = -1
            else:
                value = int(value) - 1
        except:
            dispatcher.utter_message(text="RetractPointsError, points is not an int")
        return [SlotSet("feedback_value", int(value))]

class ActionRevertUtterence(Action):
    def name(self):
        return "action_rewind_conv"

    def run(self, domain, tracker, dispatcher):
        for item in reversed(tracker.current_state()["events"]):
            if item["event"] == "user":
                tracker.current_state()["events"].pop()
                break
            tracker.current_state()["events"].pop()
        return [UserUtteranceReverted(), ActionReverted()]
    
class SetSlotHeadphones(Action):
    def name(self):
        return "action_setslot_headphones"
    
    def run(self, dispatcher, tracker, domain):
        return [SlotSet("product", "headphones")]


class ResetSlots(Action):
    def name(self):
        return "action_reset_slots"
        
    def run(self, dispatcher, tracker, domain):
        return [AllSlotsReset()]

class ActionBuyProduct(Action):
    def name(self):
        return "action_buy_product"
    
    def run(self, dispatcher, tracker, domain):
        product = tracker.current_slot_values().get("product")
        print(product)
        if product is None:
            dispatcher.utter_message(text="jag skulle vilja köpa dessa hörlurar")
            return [SlotSet("product", "headphones")]
        elif product == "headphones":
            text_out = "jag skulle vilja köpa dom här hörlurarna"
            dispatcher.utter_message(text=text_out)
            return []
        elif product == "phone":
            text_out = "jag skulle vilja köpa denna telefon"
            dispatcher.utter_message(text=text_out)
            return []
        else:
            text_out = "jag skulle vilja köpa ..."
            dispatcher.utter_message(text=text_out)

class ActionRightPrice(Action):
    def name(self):
        return "action_right_price"

    def run(self, dispatcher, tracker, domain):
        product = tracker.current_slot_values().get("product")
        if product is None:
            product = "headphones"
        if product == "headphones":
            price = PRICE_HEADPHONES
        elif product == "phone":
            price = PRICE_PHONE
        elif product == "tvinternet":
            price = PRICE_TVINTERNET
        else:
            dispatcher.utter_message(text= "Error, produkten inte inlagd")
        latest_message = tracker.latest_message
        said_price = None
        for ent in latest_message['entities']:
            if str(ent['entity']) == 'number':
                said_price = ent['value']
                said_price = int(re.sub("\D", "", said_price))
        if said_price is None:
            return [SlotSet("product", str(product))]  
        else:
            if said_price > price + 10:
                return [SlotSet("product", str(product)), SlotSet("price", "high")] 
            elif said_price < price - 100:
                return [SlotSet("product", str(product)), SlotSet("price", "low")] 
            else:
                return [SlotSet("product", str(product)), SlotSet("price", "right")] 
          
                
class ActionTellPrice(Action):
    def name(self):
        return "action_tell_price"
    
    def run(self, dispatcher, tracker, domain):
        product = tracker.current_slot_values().get("product")
        if product == "headphones":
            price = PRICE_HEADPHONES
            text_out = "Som jag förstår det borde hörlurarna kosta " + str(price) + " kronor"
        elif product == "phone":
            price = PRICE_PHONE
            text_out = "Jag tror telefonen ska kosta " + str(price) + " kronor"
        elif product == "tvinternet":
            price = PRICE_TVINTERNET
            text_out = "Jag tror det ska kosta " + str(price) + " kronor"
        else:
            text_out = "Är inte mitt jobb att berätta priset på produkten"
        dispatcher.utter_message(text = text_out)
        return []

class ActionRestart(Action):
    def name(self):
        return "action_restart"
    
    def run(self,dispatcher, tracker, domain):
        dispatcher.utter_message(text = "Omstart...")
        dispatcher.utter_message(template = "utter_intro")
        return [AllSlotsReset(), Restarted(), FollowupAction(name="action_listen")]

class ActionGuidedFallback(Action):
    def name(self) -> Text:
        return "action_guided_fallback"

    def run(self, dispatcher, tracker, domain):
        key = "default"
        fallbackN = 0
        #print("-------------")
        if len(tracker.events)>=4:
            key = "fallback0_intro"
        fallbacks = []
        for item in reversed(tracker.events):
            if item["event"] == "action":
                print(item["name"])
                if item["policy"] == "policy_1_FallbackPolicy":
                    fallbackN += 1
                if item["name"] == "action_restart":
                    key = "fallback0_intro"
                    break
                elif item["name"][0:5] == "utter":
                    last_uttering = item["name"][5:]
                    ind = 0
                    for keys in FALLBACK_DICTIONARY:
                        if keys[9:] == last_uttering:
                            fallback = "fallback" + str(ind)
                            fallbacks.append(fallback)
                            ind =+ 1
                    break
        #print("Consecutive Fallbacks: ", fallbackN)    
        #print("fallback list: ",fallbacks)
        if len(fallbacks) == 0:
            if fallbackN > len(fallbacks):
                dispatcher.utter_message(text = "Jag repeterar")
        else:
            if fallbackN >= len(fallbacks):
                dispatcher.utter_message(text = "Jag repeterar")
                key = fallbacks.pop() + last_uttering
            else:
                key = fallbacks[fallbackN] + last_uttering
        try:
            dispatcher.utter_message(text = FALLBACK_DICTIONARY[key])
        except:
            dispatcher.utter_message(template="utter_default")
        return [UserUtteranceReverted()]    

class ActionDefaultFallback(Action):
    def name(self) -> Text:
        return "action_default_fallback"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[EventType]:
        # Fallback caused by TwoStageFallbackPolicy
        if (
            len(tracker.events) >= 4
            and tracker.events[-4].get("name") == "action_default_ask_affirmation"
        ):

            dispatcher.utter_message(template="utter_restart_with_button")
            return [SlotSet("feedback_value", "negative"), ConversationPaused()]

        # Fallback caused by Core
        else:
            #print("Default Fallback")
            dispatcher.utter_message(template="utter_default")
            return [UserUtteranceReverted()]

#class ActionDefaultAskAffirmation(Action):
#    # Asks for an affirmation of the intent if NLU threshold is not met.
#
#    def name(self) -> Text:
#        return "action_default_ask_affirmation"
#
#    def __init__(self) -> None:
#        import pandas as pd
#
#        self.intent_mappings = pd.read_csv(INTENT_DESCRIPTION_MAPPING_PATH)
#        self.intent_mappings.fillna("", inplace=True)
#        self.intent_mappings.entities = self.intent_mappings.entities.map(
#            lambda entities: {e.strip() for e in entities.split(",")}
#        )
#
#    def run(
#        self,
#        dispatcher: CollectingDispatcher,
#        tracker: Tracker,
#        domain: Dict[Text, Any],
#    ) -> List[EventType]:
#        intent_ranking = tracker.latest_message.get("intent_ranking", [])
#        if len(intent_ranking) > 1:
#            diff_intent_confidence = intent_ranking[0].get(
#                "confidence"
#            ) - intent_ranking[1].get("confidence")
#            if diff_intent_confidence < 0.2:
#                intent_ranking = intent_ranking[:2]
#            else:
#                intent_ranking = intent_ranking[:1]
#        
#        # for the intent name used to retrieve the button title, we either use
#        # the name of the name of the "main" intent, or if it's an intent that triggers
#        # the response selector, we use the full retrieval intent name so that we
#        # can distinguish between the different sub intents
#        first_intent_names = [
#            intent.get("name", "")
#            if intent.get("name", "") not in ["out_of_scope", "chitchat"]
#            else tracker.latest_message.get("response_selector")
#            .get(intent.get("name", ""))
#            .get("full_retrieval_intent")
#            for intent in intent_ranking
#        ]
#        message_title = (
#            "Jag är ledsen men jag tror inte att " "förstod dig riktigt, menade du..."
#        )
#
#        entities = tracker.latest_message.get("entities", [])
#        entities = {e["entity"]: e["value"] for e in entities}
#
#        entities_json = json.dumps(entities)
#
#        buttons = []
#        for intent in first_intent_names:
#            button_title = self.get_button_title(intent, entities)
#            if "/" in intent:
#                # here we use the button title as the payload as well, because you
#                # can't force a response selector sub intent, so we need NLU to parse
#                # that correctly
#                buttons.append({"title": button_title, "payload": button_title})
#            else:
#                buttons.append(
#                    {"title": button_title, "payload": f"/{intent}{entities_json}"}
#                )
#
#        buttons.append({"title": "Jag menade något annat", "payload": "/trigger_rephrase"})
#
#        dispatcher.utter_message(text=message_title, buttons=buttons)
#
#        return []
#
#    def get_button_title(self, intent: Text, entities: Dict[Text, Text]) -> Text:
#        default_utterance_query = self.intent_mappings.intent == intent
#        utterance_query = (self.intent_mappings.entities == entities.keys()) & (
#            default_utterance_query
#        )
#
#        utterances = self.intent_mappings[utterance_query].button.tolist()
#
#        if len(utterances) > 0:
#            button_title = utterances[0]
#        else:
#            utterances = self.intent_mappings[default_utterance_query].button.tolist()
#            button_title = utterances[0] if len(utterances) > 0 else intent
#
#        return button_title.format(**entities)